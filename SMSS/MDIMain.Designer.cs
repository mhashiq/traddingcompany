﻿namespace SMSS
{
    partial class MDIMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.staffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addStaffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.standerdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classNameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.feesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feesCollectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeMethodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeVaoucherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.examsAndResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SideMenuBarpanel = new System.Windows.Forms.Panel();
            this.SideMenutableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Dashboardbutton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.DateTimetoolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.SideMenuBarpanel.SuspendLayout();
            this.SideMenutableLayoutPanel.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.classesToolStripMenuItem,
            this.feesToolStripMenuItem,
            this.examsAndResultsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(941, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.staffToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.fileToolStripMenuItem.Image = global::SMSS.Properties.Resources.document_icon_48;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // staffToolStripMenuItem
            // 
            this.staffToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.roleToolStripMenuItem,
            this.addStaffToolStripMenuItem});
            this.staffToolStripMenuItem.Image = global::SMSS.Properties.Resources.User_icon;
            this.staffToolStripMenuItem.Name = "staffToolStripMenuItem";
            this.staffToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.staffToolStripMenuItem.Text = "Staff";
            // 
            // roleToolStripMenuItem
            // 
            this.roleToolStripMenuItem.Image = global::SMSS.Properties.Resources.check_list_icon_timetable_48;
            this.roleToolStripMenuItem.Name = "roleToolStripMenuItem";
            this.roleToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.roleToolStripMenuItem.Text = "Role";
            this.roleToolStripMenuItem.Click += new System.EventHandler(this.roleToolStripMenuItem_Click);
            // 
            // addStaffToolStripMenuItem
            // 
            this.addStaffToolStripMenuItem.Image = global::SMSS.Properties.Resources.Add_user_icon_48;
            this.addStaffToolStripMenuItem.Name = "addStaffToolStripMenuItem";
            this.addStaffToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.addStaffToolStripMenuItem.Text = "Add Staff";
            // 
            // classesToolStripMenuItem
            // 
            this.classesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.classNameToolStripMenuItem,
            this.standerdToolStripMenuItem,
            this.classNameToolStripMenuItem1});
            this.classesToolStripMenuItem.Image = global::SMSS.Properties.Resources.Home_icon_48;
            this.classesToolStripMenuItem.Name = "classesToolStripMenuItem";
            this.classesToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.classesToolStripMenuItem.Text = "Classes";
            // 
            // classNameToolStripMenuItem
            // 
            this.classNameToolStripMenuItem.Image = global::SMSS.Properties.Resources.check_list_icon_timetable_481;
            this.classNameToolStripMenuItem.Name = "classNameToolStripMenuItem";
            this.classNameToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.classNameToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.classNameToolStripMenuItem.Text = "Section";
            this.classNameToolStripMenuItem.Click += new System.EventHandler(this.classNameToolStripMenuItem_Click);
            // 
            // standerdToolStripMenuItem
            // 
            this.standerdToolStripMenuItem.Image = global::SMSS.Properties.Resources.Science_School_icon_48;
            this.standerdToolStripMenuItem.Name = "standerdToolStripMenuItem";
            this.standerdToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.L)));
            this.standerdToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.standerdToolStripMenuItem.Text = "Standerd";
            // 
            // classNameToolStripMenuItem1
            // 
            this.classNameToolStripMenuItem1.Image = global::SMSS.Properties.Resources.Home_icon_48;
            this.classNameToolStripMenuItem1.Name = "classNameToolStripMenuItem1";
            this.classNameToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.classNameToolStripMenuItem1.Size = new System.Drawing.Size(217, 22);
            this.classNameToolStripMenuItem1.Text = "Class Name";
            this.classNameToolStripMenuItem1.Click += new System.EventHandler(this.classNameToolStripMenuItem1_Click);
            // 
            // feesToolStripMenuItem
            // 
            this.feesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.feesCollectToolStripMenuItem,
            this.feeMethodToolStripMenuItem,
            this.feeVaoucherToolStripMenuItem,
            this.feeCollectionToolStripMenuItem});
            this.feesToolStripMenuItem.Image = global::SMSS.Properties.Resources.budget_icon_48;
            this.feesToolStripMenuItem.Name = "feesToolStripMenuItem";
            this.feesToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.feesToolStripMenuItem.Text = "Fees";
            // 
            // feesCollectToolStripMenuItem
            // 
            this.feesCollectToolStripMenuItem.Image = global::SMSS.Properties.Resources.pyment_48;
            this.feesCollectToolStripMenuItem.Name = "feesCollectToolStripMenuItem";
            this.feesCollectToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.feesCollectToolStripMenuItem.Text = "Fee Structure";
            // 
            // feeMethodToolStripMenuItem
            // 
            this.feeMethodToolStripMenuItem.Image = global::SMSS.Properties.Resources.pyment_48;
            this.feeMethodToolStripMenuItem.Name = "feeMethodToolStripMenuItem";
            this.feeMethodToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.feeMethodToolStripMenuItem.Text = "Fee Method";
            // 
            // feeVaoucherToolStripMenuItem
            // 
            this.feeVaoucherToolStripMenuItem.Image = global::SMSS.Properties.Resources.fax_machine_icon_48;
            this.feeVaoucherToolStripMenuItem.Name = "feeVaoucherToolStripMenuItem";
            this.feeVaoucherToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.feeVaoucherToolStripMenuItem.Text = "Fee Vaoucher";
            // 
            // feeCollectionToolStripMenuItem
            // 
            this.feeCollectionToolStripMenuItem.Image = global::SMSS.Properties.Resources.paper_currency_icon_48;
            this.feeCollectionToolStripMenuItem.Name = "feeCollectionToolStripMenuItem";
            this.feeCollectionToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.feeCollectionToolStripMenuItem.Text = "Fee Collection";
            // 
            // examsAndResultsToolStripMenuItem
            // 
            this.examsAndResultsToolStripMenuItem.Image = global::SMSS.Properties.Resources.Edit_Map_icon_48;
            this.examsAndResultsToolStripMenuItem.Name = "examsAndResultsToolStripMenuItem";
            this.examsAndResultsToolStripMenuItem.Size = new System.Drawing.Size(138, 20);
            this.examsAndResultsToolStripMenuItem.Text = "Exams and Results";
            // 
            // SideMenuBarpanel
            // 
            this.SideMenuBarpanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            this.SideMenuBarpanel.Controls.Add(this.SideMenutableLayoutPanel);
            this.SideMenuBarpanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.SideMenuBarpanel.Location = new System.Drawing.Point(794, 24);
            this.SideMenuBarpanel.Name = "SideMenuBarpanel";
            this.SideMenuBarpanel.Size = new System.Drawing.Size(147, 426);
            this.SideMenuBarpanel.TabIndex = 5;
            // 
            // SideMenutableLayoutPanel
            // 
            this.SideMenutableLayoutPanel.BackColor = System.Drawing.Color.White;
            this.SideMenutableLayoutPanel.ColumnCount = 1;
            this.SideMenutableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.SideMenutableLayoutPanel.Controls.Add(this.button5, 0, 4);
            this.SideMenutableLayoutPanel.Controls.Add(this.button4, 0, 3);
            this.SideMenutableLayoutPanel.Controls.Add(this.button3, 0, 2);
            this.SideMenutableLayoutPanel.Controls.Add(this.button2, 0, 1);
            this.SideMenutableLayoutPanel.Controls.Add(this.button1, 0, 0);
            this.SideMenutableLayoutPanel.Controls.Add(this.Dashboardbutton, 0, 5);
            this.SideMenutableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SideMenutableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.SideMenutableLayoutPanel.Name = "SideMenutableLayoutPanel";
            this.SideMenutableLayoutPanel.RowCount = 9;
            this.SideMenutableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.SideMenutableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.SideMenutableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.SideMenutableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.SideMenutableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.SideMenutableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.SideMenutableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.SideMenutableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.SideMenutableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.SideMenutableLayoutPanel.Size = new System.Drawing.Size(147, 426);
            this.SideMenutableLayoutPanel.TabIndex = 2;
            // 
            // button5
            // 
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = global::SMSS.Properties.Resources.Settings_icon_32;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(3, 191);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(141, 41);
            this.button5.TabIndex = 4;
            this.button5.Text = "Settings";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::SMSS.Properties.Resources.Search_icon_32;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(3, 144);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(141, 41);
            this.button4.TabIndex = 3;
            this.button4.Text = "Search Student";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = global::SMSS.Properties.Resources.fax_machine_icon_24;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(3, 97);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(141, 41);
            this.button3.TabIndex = 2;
            this.button3.Text = "Print Fee Slip";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::SMSS.Properties.Resources.budget_icon_32;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(3, 50);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(141, 41);
            this.button2.TabIndex = 1;
            this.button2.Text = "Process Fee";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::SMSS.Properties.Resources.Add_user_icon_32;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 41);
            this.button1.TabIndex = 0;
            this.button1.Text = "New Student";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Dashboardbutton
            // 
            this.Dashboardbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Dashboardbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Dashboardbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Dashboardbutton.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dashboardbutton.Image = global::SMSS.Properties.Resources.Home_icon_32;
            this.Dashboardbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Dashboardbutton.Location = new System.Drawing.Point(3, 238);
            this.Dashboardbutton.Name = "Dashboardbutton";
            this.Dashboardbutton.Size = new System.Drawing.Size(141, 41);
            this.Dashboardbutton.TabIndex = 5;
            this.Dashboardbutton.Text = "Dashboard";
            this.Dashboardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Dashboardbutton.UseVisualStyleBackColor = true;
            this.Dashboardbutton.Click += new System.EventHandler(this.Dashboardbutton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.DateTimetoolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 426);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(794, 24);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 19);
            // 
            // DateTimetoolStripStatusLabel
            // 
            this.DateTimetoolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.DateTimetoolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
            this.DateTimetoolStripStatusLabel.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTimetoolStripStatusLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            this.DateTimetoolStripStatusLabel.Name = "DateTimetoolStripStatusLabel";
            this.DateTimetoolStripStatusLabel.Size = new System.Drawing.Size(66, 19);
            this.DateTimetoolStripStatusLabel.Text = "DateTime";
            this.DateTimetoolStripStatusLabel.Click += new System.EventHandler(this.DateTimetoolStripStatusLabel_Click);
            // 
            // MDIMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(941, 450);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.SideMenuBarpanel);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MDIMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MDIMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.SideMenuBarpanel.ResumeLayout(false);
            this.SideMenutableLayoutPanel.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Panel SideMenuBarpanel;
        private System.Windows.Forms.TableLayoutPanel SideMenutableLayoutPanel;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Dashboardbutton;
        private System.Windows.Forms.ToolStripMenuItem classesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem standerdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classNameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem feesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feesCollectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeMethodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeVaoucherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem staffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addStaffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem examsAndResultsToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel DateTimetoolStripStatusLabel;
    }
}

