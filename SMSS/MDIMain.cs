﻿using SMSS.Screens;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS
{
    public partial class MDIMain : Form
    {
        public MDIMain()
        {
            InitializeComponent();
        }

        private void MDIMain_Load(object sender, EventArgs e)
        {
            DateTimetoolStripStatusLabel.Text = DateTime.Now.ToString();
            MdiClient ctlMdi;
            foreach(Control ctr in this.Controls)
            {
                try
                {
                    ctlMdi = (MdiClient)ctr;
                    ctlMdi.BackColor = System.Drawing.Color.White;
                   // ctlMdi.BackgroundImage = Properties.Resources.schoolimage;
                    
                }
                catch(InvalidCastException)
                {

                }
            }
        }

        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings s = new Settings();
            s.WindowState = FormWindowState.Normal;
            s.MdiParent = this;
            //s.MdiChildren = this;
            s.Show();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainLoginScreen obj = new MainLoginScreen();
            obj.WindowState = FormWindowState.Normal;
            obj.MdiParent = this;
            obj.Show();
        }

        private void Dashboardbutton_Click(object sender, EventArgs e)
        {
            
            Dashboard d = new Dashboard();
            d.MdiParent = this;
            d.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Dashboard d = new Dashboard();
            d.MdiParent = this;
            d.Show();
        }

        private void DateTimetoolStripStatusLabel_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //AdmissionForm a = new AdmissionForm();
            //a.MdiParent = this;
            //a.Show();
        }

        private void roleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Roles a = new Roles();
            a.MdiParent = this;
            a.Show();
        }

        private void classNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Section s = new Section();
            s.MdiParent = this;
            s.Show();
        }

        private void classNameToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Standerd s = new Standerd();
            s.MdiParent = this;
            s.Show();
        }
    }
}
