﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS.Screens
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            DSlabel.Visible = false;
            DNlabel11.Visible = false;
            UNlabel12.Visible = false;
            PSDlabel13.Visible = false;
        }



        private void DStextBox_TextChanged(object sender, EventArgs e)
        {
            if (DStextBox.Text == "") { DSlabel.Visible = true; } else { DSlabel.Visible = false; }
        }

        private void DNtextBox_TextChanged(object sender, EventArgs e)
        {
            if (DNtextBox.Text == "") { DNlabel11.Visible = true; } else { DNlabel11.Visible = false; }
        }

        private void UNtextBox_TextChanged(object sender, EventArgs e)
        {
            if (UNtextBox.Text == "") { UNlabel12.Visible = true; } else { UNlabel12.Visible = false; }
        }

        private void PSDtextBox_TextChanged(object sender, EventArgs e)
        {
            if (PSDtextBox.Text == "") { PSDlabel13.Visible = true; } else { PSDlabel13.Visible = false; }
        }

        private void Svdbutton_Click(object sender, EventArgs e)
        {
            StringBuilder sb=new StringBuilder();
            if (IScheckBox1.Checked == true)
            {
                if (DStextBox.Text == "" || DNtextBox.Text == "")
                {

                    MessageBox.Show("All (*) field are required!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    UNtextBox.Clear();
                    PSDtextBox.Clear();
                    UNtextBox.Enabled = false;
                    PSDtextBox.Enabled = false;
                    sb.Append("Data Source="+DStextBox.Text+";Initial Catalog="+DNtextBox.Text+";Intigrity Security=true;");
                    if(!File.Exists(UtitlityClass.path+"\\connect"))
                    {
                        File.WriteAllText(UtitlityClass.path+"\\connect",sb.ToString());
                    }
                    MessageBox.Show("Successful added!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                if (DStextBox.Text == "" || DNtextBox.Text == "" || UNtextBox.Text == "" || PSDtextBox.Text == "")
                {
                    MessageBox.Show("All (*) field are required!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                    MessageBox.Show("Successful added!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void IScheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(IScheckBox1.Checked)
            {
                UNtextBox.Clear();
                PSDtextBox.Clear();
                UNtextBox.Enabled = false;
                PSDtextBox.Enabled = false;
            }
            else
            {
                UNtextBox.Enabled = true;
                PSDtextBox.Enabled = true;
            }
        }

        private void ClosepictureBox_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MinipictureBox_Click(object sender, EventArgs e)
        {
            if(this.WindowState!=FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }
    }
}
