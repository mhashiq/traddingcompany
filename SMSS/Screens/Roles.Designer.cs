﻿namespace SMSS.Screens
{
    partial class Roles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxgroupBox = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.GroupBoxpanel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.RoledataGridView = new System.Windows.Forms.DataGridView();
            this.snoGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rolesIDGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rolesNameGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Linepanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.TextGroupBoxpanel = new System.Windows.Forms.Panel();
            this.TextControlgroupBox = new System.Windows.Forms.GroupBox();
            this.StatuscomboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.RNametextBox = new System.Windows.Forms.TextBox();
            this.TextBoxgroupBox.SuspendLayout();
            this.GroupBoxpanel.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoledataGridView)).BeginInit();
            this.Linepanel.SuspendLayout();
            this.TextGroupBoxpanel.SuspendLayout();
            this.TextControlgroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Toppanel
            // 
            this.Toppanel.Size = new System.Drawing.Size(552, 76);
            // 
            // ButtonControlpanel
            // 
            this.ButtonControlpanel.Size = new System.Drawing.Size(552, 53);
            // 
            // Centerpanel
            // 
            this.Centerpanel.Size = new System.Drawing.Size(552, 223);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(148, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "In Add Role ";
            // 
            // TextBoxgroupBox
            // 
            this.TextBoxgroupBox.Controls.Add(this.textBox1);
            this.TextBoxgroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBoxgroupBox.Location = new System.Drawing.Point(0, 0);
            this.TextBoxgroupBox.Name = "TextBoxgroupBox";
            this.TextBoxgroupBox.Size = new System.Drawing.Size(636, 83);
            this.TextBoxgroupBox.TabIndex = 1;
            this.TextBoxgroupBox.TabStop = false;
            this.TextBoxgroupBox.Text = "Add Role";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(100, 37);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(168, 20);
            this.textBox1.TabIndex = 0;
            // 
            // GroupBoxpanel
            // 
            this.GroupBoxpanel.Controls.Add(this.TextBoxgroupBox);
            this.GroupBoxpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupBoxpanel.Location = new System.Drawing.Point(0, 54);
            this.GroupBoxpanel.Name = "GroupBoxpanel";
            this.GroupBoxpanel.Size = new System.Drawing.Size(636, 83);
            this.GroupBoxpanel.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 137);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(636, 206);
            this.panel2.TabIndex = 3;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(636, 206);
            this.dataGridView1.TabIndex = 0;
            // 
            // RoledataGridView
            // 
            this.RoledataGridView.AllowUserToAddRows = false;
            this.RoledataGridView.AllowUserToDeleteRows = false;
            this.RoledataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RoledataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.RoledataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.RoledataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RoledataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.snoGV,
            this.rolesIDGV,
            this.rolesNameGV,
            this.statusGV});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RoledataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.RoledataGridView.Location = new System.Drawing.Point(1, 228);
            this.RoledataGridView.Name = "RoledataGridView";
            this.RoledataGridView.ReadOnly = true;
            this.RoledataGridView.RowHeadersVisible = false;
            this.RoledataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.RoledataGridView.Size = new System.Drawing.Size(551, 124);
            this.RoledataGridView.TabIndex = 2;
            this.RoledataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.RoledataGridView_CellClick);
            // 
            // snoGV
            // 
            this.snoGV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.snoGV.HeaderText = "S.No";
            this.snoGV.Name = "snoGV";
            this.snoGV.ReadOnly = true;
            this.snoGV.Width = 56;
            // 
            // rolesIDGV
            // 
            this.rolesIDGV.HeaderText = "RolesID";
            this.rolesIDGV.Name = "rolesIDGV";
            this.rolesIDGV.ReadOnly = true;
            this.rolesIDGV.Visible = false;
            // 
            // rolesNameGV
            // 
            this.rolesNameGV.HeaderText = "Role";
            this.rolesNameGV.Name = "rolesNameGV";
            this.rolesNameGV.ReadOnly = true;
            // 
            // statusGV
            // 
            this.statusGV.HeaderText = "Status";
            this.statusGV.Name = "statusGV";
            this.statusGV.ReadOnly = true;
            // 
            // Linepanel
            // 
            this.Linepanel.Controls.Add(this.label3);
            this.Linepanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Linepanel.Location = new System.Drawing.Point(0, 129);
            this.Linepanel.Name = "Linepanel";
            this.Linepanel.Size = new System.Drawing.Size(552, 13);
            this.Linepanel.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(829, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "_________________________________________________________________________________" +
    "________________________________________________________";
            // 
            // TextGroupBoxpanel
            // 
            this.TextGroupBoxpanel.Controls.Add(this.TextControlgroupBox);
            this.TextGroupBoxpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TextGroupBoxpanel.Location = new System.Drawing.Point(0, 142);
            this.TextGroupBoxpanel.Name = "TextGroupBoxpanel";
            this.TextGroupBoxpanel.Size = new System.Drawing.Size(552, 81);
            this.TextGroupBoxpanel.TabIndex = 4;
            // 
            // TextControlgroupBox
            // 
            this.TextControlgroupBox.Controls.Add(this.StatuscomboBox);
            this.TextControlgroupBox.Controls.Add(this.label5);
            this.TextControlgroupBox.Controls.Add(this.label4);
            this.TextControlgroupBox.Controls.Add(this.RNametextBox);
            this.TextControlgroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextControlgroupBox.Location = new System.Drawing.Point(0, 0);
            this.TextControlgroupBox.Name = "TextControlgroupBox";
            this.TextControlgroupBox.Size = new System.Drawing.Size(552, 81);
            this.TextControlgroupBox.TabIndex = 0;
            this.TextControlgroupBox.TabStop = false;
            this.TextControlgroupBox.Text = "Role Entry ";
            // 
            // StatuscomboBox
            // 
            this.StatuscomboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.StatuscomboBox.FormattingEnabled = true;
            this.StatuscomboBox.Items.AddRange(new object[] {
            "Active",
            "In-Active"});
            this.StatuscomboBox.Location = new System.Drawing.Point(123, 57);
            this.StatuscomboBox.Name = "StatuscomboBox";
            this.StatuscomboBox.Size = new System.Drawing.Size(344, 21);
            this.StatuscomboBox.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(58, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Enter Role:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Enter Role:";
            // 
            // RNametextBox
            // 
            this.RNametextBox.Location = new System.Drawing.Point(123, 25);
            this.RNametextBox.Name = "RNametextBox";
            this.RNametextBox.Size = new System.Drawing.Size(344, 20);
            this.RNametextBox.TabIndex = 0;
            // 
            // Roles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 352);
            this.Controls.Add(this.TextGroupBoxpanel);
            this.Controls.Add(this.Linepanel);
            this.Controls.Add(this.RoledataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Roles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Roles";
            this.Load += new System.EventHandler(this.Roles_Load);
            this.Controls.SetChildIndex(this.Toppanel, 0);
            this.Controls.SetChildIndex(this.ButtonControlpanel, 0);
            this.Controls.SetChildIndex(this.Centerpanel, 0);
            this.Controls.SetChildIndex(this.RoledataGridView, 0);
            this.Controls.SetChildIndex(this.Linepanel, 0);
            this.Controls.SetChildIndex(this.TextGroupBoxpanel, 0);
            this.TextBoxgroupBox.ResumeLayout(false);
            this.TextBoxgroupBox.PerformLayout();
            this.GroupBoxpanel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoledataGridView)).EndInit();
            this.Linepanel.ResumeLayout(false);
            this.Linepanel.PerformLayout();
            this.TextGroupBoxpanel.ResumeLayout(false);
            this.TextControlgroupBox.ResumeLayout(false);
            this.TextControlgroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel GroupBoxpanel;
        private System.Windows.Forms.GroupBox TextBoxgroupBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView RoledataGridView;
        private System.Windows.Forms.Panel Linepanel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel TextGroupBoxpanel;
        private System.Windows.Forms.GroupBox TextControlgroupBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox RNametextBox;
        private System.Windows.Forms.ComboBox StatuscomboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn snoGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn rolesIDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn rolesNameGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusGV;
    }
}