﻿namespace SMSS.Screens
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.IScheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DSrcelabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.DataSourcetextBox = new System.Windows.Forms.TextBox();
            this.DataBasetextBox = new System.Windows.Forms.TextBox();
            this.UserNametextBox = new System.Windows.Forms.TextBox();
            this.PasswordtextBox = new System.Windows.Forms.TextBox();
            this.DNlabel = new System.Windows.Forms.Label();
            this.UNlabel = new System.Windows.Forms.Label();
            this.PSDlabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Svdbutton = new System.Windows.Forms.Button();
            this.DStextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DNtextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.UNtextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.PSDtextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.IScheckBox1 = new System.Windows.Forms.CheckBox();
            this.DSlabel = new System.Windows.Forms.Label();
            this.DNlabel11 = new System.Windows.Forms.Label();
            this.UNlabel12 = new System.Windows.Forms.Label();
            this.PSDlabel13 = new System.Windows.Forms.Label();
            this.ClosepictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Savebutton = new System.Windows.Forms.Button();
            this.MinipictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ClosepictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinipictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 278);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Data Source:";
            // 
            // IScheckBox
            // 
            this.IScheckBox.AutoSize = true;
            this.IScheckBox.Location = new System.Drawing.Point(32, 453);
            this.IScheckBox.Name = "IScheckBox";
            this.IScheckBox.Size = new System.Drawing.Size(100, 17);
            this.IScheckBox.TabIndex = 6;
            this.IScheckBox.Text = "Intigrity Security";
            this.IScheckBox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 325);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Database Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 367);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "User Name:";
            // 
            // DSrcelabel
            // 
            this.DSrcelabel.AutoSize = true;
            this.DSrcelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DSrcelabel.Location = new System.Drawing.Point(161, 282);
            this.DSrcelabel.Name = "DSrcelabel";
            this.DSrcelabel.Size = new System.Drawing.Size(16, 20);
            this.DSrcelabel.TabIndex = 12;
            this.DSrcelabel.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 411);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Password:";
            // 
            // DataSourcetextBox
            // 
            this.DataSourcetextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataSourcetextBox.Location = new System.Drawing.Point(28, 294);
            this.DataSourcetextBox.Multiline = true;
            this.DataSourcetextBox.Name = "DataSourcetextBox";
            this.DataSourcetextBox.Size = new System.Drawing.Size(150, 20);
            this.DataSourcetextBox.TabIndex = 14;
            // 
            // DataBasetextBox
            // 
            this.DataBasetextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataBasetextBox.Location = new System.Drawing.Point(28, 341);
            this.DataBasetextBox.Multiline = true;
            this.DataBasetextBox.Name = "DataBasetextBox";
            this.DataBasetextBox.Size = new System.Drawing.Size(150, 20);
            this.DataBasetextBox.TabIndex = 14;
            // 
            // UserNametextBox
            // 
            this.UserNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UserNametextBox.Location = new System.Drawing.Point(28, 383);
            this.UserNametextBox.Multiline = true;
            this.UserNametextBox.Name = "UserNametextBox";
            this.UserNametextBox.Size = new System.Drawing.Size(150, 20);
            this.UserNametextBox.TabIndex = 14;
            // 
            // PasswordtextBox
            // 
            this.PasswordtextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PasswordtextBox.Location = new System.Drawing.Point(28, 427);
            this.PasswordtextBox.Multiline = true;
            this.PasswordtextBox.Name = "PasswordtextBox";
            this.PasswordtextBox.Size = new System.Drawing.Size(150, 20);
            this.PasswordtextBox.TabIndex = 14;
            // 
            // DNlabel
            // 
            this.DNlabel.AutoSize = true;
            this.DNlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DNlabel.Location = new System.Drawing.Point(159, 329);
            this.DNlabel.Name = "DNlabel";
            this.DNlabel.Size = new System.Drawing.Size(16, 20);
            this.DNlabel.TabIndex = 15;
            this.DNlabel.Text = "*";
            // 
            // UNlabel
            // 
            this.UNlabel.AutoSize = true;
            this.UNlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UNlabel.Location = new System.Drawing.Point(159, 369);
            this.UNlabel.Name = "UNlabel";
            this.UNlabel.Size = new System.Drawing.Size(16, 20);
            this.UNlabel.TabIndex = 16;
            this.UNlabel.Text = "*";
            // 
            // PSDlabel
            // 
            this.PSDlabel.AutoSize = true;
            this.PSDlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PSDlabel.Location = new System.Drawing.Point(159, 415);
            this.PSDlabel.Name = "PSDlabel";
            this.PSDlabel.Size = new System.Drawing.Size(16, 20);
            this.PSDlabel.TabIndex = 17;
            this.PSDlabel.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(226, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 24);
            this.label4.TabIndex = 1;
            this.label4.Text = "Db Connection";
            // 
            // Svdbutton
            // 
            this.Svdbutton.Location = new System.Drawing.Point(100, 432);
            this.Svdbutton.Name = "Svdbutton";
            this.Svdbutton.Size = new System.Drawing.Size(217, 35);
            this.Svdbutton.TabIndex = 2;
            this.Svdbutton.Text = "Save";
            this.Svdbutton.UseVisualStyleBackColor = true;
            this.Svdbutton.Click += new System.EventHandler(this.Svdbutton_Click);
            // 
            // DStextBox
            // 
            this.DStextBox.Location = new System.Drawing.Point(101, 231);
            this.DStextBox.Name = "DStextBox";
            this.DStextBox.Size = new System.Drawing.Size(216, 20);
            this.DStextBox.TabIndex = 3;
            this.DStextBox.TextChanged += new System.EventHandler(this.DStextBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(100, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Data Source:";
            // 
            // DNtextBox
            // 
            this.DNtextBox.Location = new System.Drawing.Point(100, 284);
            this.DNtextBox.Name = "DNtextBox";
            this.DNtextBox.Size = new System.Drawing.Size(217, 20);
            this.DNtextBox.TabIndex = 3;
            this.DNtextBox.TextChanged += new System.EventHandler(this.DNtextBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(102, 266);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Database Name:";
            // 
            // UNtextBox
            // 
            this.UNtextBox.Location = new System.Drawing.Point(100, 324);
            this.UNtextBox.Name = "UNtextBox";
            this.UNtextBox.Size = new System.Drawing.Size(217, 20);
            this.UNtextBox.TabIndex = 3;
            this.UNtextBox.TextChanged += new System.EventHandler(this.UNtextBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(102, 306);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 15);
            this.label7.TabIndex = 4;
            this.label7.Text = "User Name:";
            // 
            // PSDtextBox
            // 
            this.PSDtextBox.Location = new System.Drawing.Point(100, 367);
            this.PSDtextBox.Name = "PSDtextBox";
            this.PSDtextBox.Size = new System.Drawing.Size(217, 20);
            this.PSDtextBox.TabIndex = 3;
            this.PSDtextBox.TextChanged += new System.EventHandler(this.PSDtextBox_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(102, 349);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 15);
            this.label8.TabIndex = 4;
            this.label8.Text = "Password:";
            // 
            // IScheckBox1
            // 
            this.IScheckBox1.AutoSize = true;
            this.IScheckBox1.Location = new System.Drawing.Point(101, 402);
            this.IScheckBox1.Name = "IScheckBox1";
            this.IScheckBox1.Size = new System.Drawing.Size(100, 17);
            this.IScheckBox1.TabIndex = 5;
            this.IScheckBox1.Text = "Intigrity Security";
            this.IScheckBox1.UseVisualStyleBackColor = true;
            this.IScheckBox1.CheckedChanged += new System.EventHandler(this.IScheckBox1_CheckedChanged);
            // 
            // DSlabel
            // 
            this.DSlabel.AutoSize = true;
            this.DSlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DSlabel.Location = new System.Drawing.Point(293, 213);
            this.DSlabel.Name = "DSlabel";
            this.DSlabel.Size = new System.Drawing.Size(21, 25);
            this.DSlabel.TabIndex = 1;
            this.DSlabel.Text = "*";
            // 
            // DNlabel11
            // 
            this.DNlabel11.AutoSize = true;
            this.DNlabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DNlabel11.Location = new System.Drawing.Point(293, 266);
            this.DNlabel11.Name = "DNlabel11";
            this.DNlabel11.Size = new System.Drawing.Size(21, 25);
            this.DNlabel11.TabIndex = 1;
            this.DNlabel11.Text = "*";
            // 
            // UNlabel12
            // 
            this.UNlabel12.AutoSize = true;
            this.UNlabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UNlabel12.Location = new System.Drawing.Point(293, 306);
            this.UNlabel12.Name = "UNlabel12";
            this.UNlabel12.Size = new System.Drawing.Size(21, 25);
            this.UNlabel12.TabIndex = 1;
            this.UNlabel12.Text = "*";
            // 
            // PSDlabel13
            // 
            this.PSDlabel13.AutoSize = true;
            this.PSDlabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PSDlabel13.Location = new System.Drawing.Point(293, 349);
            this.PSDlabel13.Name = "PSDlabel13";
            this.PSDlabel13.Size = new System.Drawing.Size(21, 25);
            this.PSDlabel13.TabIndex = 1;
            this.PSDlabel13.Text = "*";
            // 
            // ClosepictureBox
            // 
            this.ClosepictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ClosepictureBox.Image = global::SMSS.Properties.Resources.System_icon_close;
            this.ClosepictureBox.Location = new System.Drawing.Point(347, 0);
            this.ClosepictureBox.Name = "ClosepictureBox";
            this.ClosepictureBox.Size = new System.Drawing.Size(50, 46);
            this.ClosepictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ClosepictureBox.TabIndex = 0;
            this.ClosepictureBox.TabStop = false;
            this.ClosepictureBox.Click += new System.EventHandler(this.ClosepictureBox_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SMSS.Properties.Resources.system_database_add_icon;
            this.pictureBox1.Location = new System.Drawing.Point(17, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(193, 184);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Savebutton
            // 
            this.Savebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Savebutton.Image = global::SMSS.Properties.Resources.Save_icon_24;
            this.Savebutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Savebutton.Location = new System.Drawing.Point(28, 476);
            this.Savebutton.Name = "Savebutton";
            this.Savebutton.Size = new System.Drawing.Size(150, 30);
            this.Savebutton.TabIndex = 4;
            this.Savebutton.Text = "Save Settings";
            this.Savebutton.UseVisualStyleBackColor = true;
            // 
            // MinipictureBox
            // 
            this.MinipictureBox.Image = global::SMSS.Properties.Resources.User_icon;
            this.MinipictureBox.Location = new System.Drawing.Point(298, 0);
            this.MinipictureBox.Name = "MinipictureBox";
            this.MinipictureBox.Size = new System.Drawing.Size(38, 42);
            this.MinipictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MinipictureBox.TabIndex = 6;
            this.MinipictureBox.TabStop = false;
            this.MinipictureBox.Click += new System.EventHandler(this.MinipictureBox_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(397, 541);
            this.Controls.Add(this.MinipictureBox);
            this.Controls.Add(this.IScheckBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.PSDtextBox);
            this.Controls.Add(this.UNtextBox);
            this.Controls.Add(this.DNtextBox);
            this.Controls.Add(this.DStextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Svdbutton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ClosepictureBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PSDlabel13);
            this.Controls.Add(this.UNlabel12);
            this.Controls.Add(this.DNlabel11);
            this.Controls.Add(this.DSlabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ClosepictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinipictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label DSrcelabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Savebutton;
        private System.Windows.Forms.CheckBox IScheckBox;
        private System.Windows.Forms.TextBox DataSourcetextBox;
        private System.Windows.Forms.TextBox PasswordtextBox;
        private System.Windows.Forms.TextBox UserNametextBox;
        private System.Windows.Forms.TextBox DataBasetextBox;
        private System.Windows.Forms.Label DNlabel;
        private System.Windows.Forms.Label UNlabel;
        private System.Windows.Forms.Label PSDlabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Svdbutton;
        private System.Windows.Forms.TextBox DStextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox DNtextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox UNtextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox PSDtextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox IScheckBox1;
        private System.Windows.Forms.Label DSlabel;
        private System.Windows.Forms.Label DNlabel11;
        private System.Windows.Forms.Label UNlabel12;
        private System.Windows.Forms.Label PSDlabel13;
        private System.Windows.Forms.PictureBox ClosepictureBox;
        private System.Windows.Forms.PictureBox MinipictureBox;
    }
}