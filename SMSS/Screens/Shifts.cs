﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS.Screens
{
    public partial class Shifts : Layout
    {
        int edit = 0;
        myDBDataContext context = new myDBDataContext();
        int id;
        public Shifts()
        {
            InitializeComponent();
        }

        private void Shifts_Load(object sender, EventArgs e)
        {

        }

        public void LoadData()
        {
            var data = context.Shift_GetShifts();
            ShGvId.DataPropertyName = "ID";
            ShGvName.DataPropertyName = "Name";
            ShGvNum.DataPropertyName = "Number";
            ShGvFromT.DataPropertyName = "FromDate";
            ShGvToT.DataPropertyName = "ToDate";
            ShiftdataGridView.DataSource = data;
            UtitlityClass.SNO(ShiftdataGridView, "ShGvSno", 0);
        }
        public override void Addbutton_Click(object sender, EventArgs e)
        {
            edit = 0;
            UtitlityClass.Enable_Reset(ShiftgroupBox);
        }

        public override void Savebutton_Click(object sender, EventArgs e)
        {
            TimeSpan startTime = new TimeSpan(FromDateTime.Value.Hour, FromDateTime.Value.Minute, FromDateTime.Value.Second);
            TimeSpan endTime = new TimeSpan(ToDateTime.Value.Hour, ToDateTime.Value.Minute, ToDateTime.Value.Second);
            if (edit == 0)
            {
                context.Shift_Insert(ShiftNametextBox.Text, Convert.ToInt32(ShiftNumtextBox.Text), startTime,endTime);
                UtitlityClass.ShowMsg("Insert Successfuly!", "Information", "Success");
                LoadData();
                UtitlityClass.Disable_Reset(ShiftgroupBox);
            }
            else if (edit == 1)
            {
                context.Shift_Update(id,ShiftNametextBox.Text, Convert.ToInt32(ShiftNumtextBox.Text), startTime,endTime);
                UtitlityClass.ShowMsg("Update Successfuly!", "Information", "Success");
                LoadData();
                UtitlityClass.Disable_Reset(ShiftgroupBox);
            }
        }

        public override void GetRecordbutton_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        public override void Updatebutton_Click(object sender, EventArgs e)
        {
            edit = 1;
            UtitlityClass.Enable(ShiftgroupBox);
        }

        public override void Delbutton_Click(object sender, EventArgs e)
        {
            context.Shift_Delete(id);
            UtitlityClass.ShowMsg("Deleted Successfuly!", "Information", "Success");
            LoadData();
            UtitlityClass.Disable_Reset(ShiftgroupBox);
        }

        public override void Clearbutton_Click(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(ShiftgroupBox);
        }

    }
}
