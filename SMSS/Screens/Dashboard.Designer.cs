﻿namespace SMSS.Screens
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button20 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.ClassSchedule = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.Shiftbutton = new System.Windows.Forms.Button();
            this.Subjectbutton = new System.Windows.Forms.Button();
            this.Clsbutton = new System.Windows.Forms.Button();
            this.Sectionbutton = new System.Windows.Forms.Button();
            this.Admissionbutton = new System.Windows.Forms.Button();
            this.Rolebutton = new System.Windows.Forms.Button();
            this.Staffbutton = new System.Windows.Forms.Button();
            this.MinimizepictureBox = new System.Windows.Forms.PictureBox();
            this.ClosepictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.TopPanel.SuspendLayout();
            this.CentralPanel.SuspendLayout();
            this.TopPanel2.SuspendLayout();
            this.LeftPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizepictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosepictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // TopPanel
            // 
            this.TopPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TopPanel.Controls.Add(this.MinimizepictureBox);
            this.TopPanel.Controls.Add(this.ClosepictureBox);
            this.TopPanel.Size = new System.Drawing.Size(790, 101);
            // 
            // CentralPanel
            // 
            this.CentralPanel.Controls.Add(this.tableLayoutPanel1);
            this.CentralPanel.Location = new System.Drawing.Point(200, 101);
            this.CentralPanel.Size = new System.Drawing.Size(790, 397);
            // 
            // TopPanel2
            // 
            this.TopPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TopPanel2.Controls.Add(this.pictureBox1);
            this.TopPanel2.Size = new System.Drawing.Size(198, 100);
            // 
            // LeftPanel
            // 
            this.LeftPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LeftPanel.Controls.Add(this.pictureBox2);
            this.LeftPanel.Size = new System.Drawing.Size(200, 498);
            this.LeftPanel.Controls.SetChildIndex(this.TopPanel2, 0);
            this.LeftPanel.Controls.SetChildIndex(this.pictureBox2, 0);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.button20, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.button19, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.button18, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.button17, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.button16, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.button15, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.button14, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.button13, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.ClassSchedule, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.button11, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button10, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.button9, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.button8, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.Shiftbutton, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Subjectbutton, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Clsbutton, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.Sectionbutton, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.Admissionbutton, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.Rolebutton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Staffbutton, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(790, 397);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.White;
            this.button20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Location = new System.Drawing.Point(635, 300);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(152, 94);
            this.button20.TabIndex = 19;
            this.button20.Text = "button20";
            this.button20.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button20.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.White;
            this.button19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.Location = new System.Drawing.Point(477, 300);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(152, 94);
            this.button19.TabIndex = 18;
            this.button19.Text = "button19";
            this.button19.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button19.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.White;
            this.button18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.Location = new System.Drawing.Point(319, 300);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(152, 94);
            this.button18.TabIndex = 17;
            this.button18.Text = "button18";
            this.button18.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button18.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.White;
            this.button17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(161, 300);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(152, 94);
            this.button17.TabIndex = 16;
            this.button17.Text = "button17";
            this.button17.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button17.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.White;
            this.button16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Location = new System.Drawing.Point(3, 300);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(152, 94);
            this.button16.TabIndex = 15;
            this.button16.Text = "button16";
            this.button16.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button16.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.White;
            this.button15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(635, 201);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(152, 93);
            this.button15.TabIndex = 14;
            this.button15.Text = "button15";
            this.button15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.White;
            this.button14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(477, 201);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(152, 93);
            this.button14.TabIndex = 13;
            this.button14.Text = "button14";
            this.button14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(319, 201);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(152, 93);
            this.button13.TabIndex = 12;
            this.button13.Text = "button13";
            this.button13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button13.UseVisualStyleBackColor = false;
            // 
            // ClassSchedule
            // 
            this.ClassSchedule.BackColor = System.Drawing.Color.White;
            this.ClassSchedule.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ClassSchedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClassSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClassSchedule.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassSchedule.Location = new System.Drawing.Point(161, 201);
            this.ClassSchedule.Name = "ClassSchedule";
            this.ClassSchedule.Size = new System.Drawing.Size(152, 93);
            this.ClassSchedule.TabIndex = 11;
            this.ClassSchedule.Text = "Class Schedule";
            this.ClassSchedule.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ClassSchedule.UseVisualStyleBackColor = false;
            this.ClassSchedule.Click += new System.EventHandler(this.ClassSchedule_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.White;
            this.button11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(3, 201);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(152, 93);
            this.button11.TabIndex = 10;
            this.button11.Text = "Groups";
            this.button11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.White;
            this.button10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(635, 102);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(152, 93);
            this.button10.TabIndex = 9;
            this.button10.Text = "Fees Management";
            this.button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.White;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(477, 102);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(152, 93);
            this.button9.TabIndex = 8;
            this.button9.Text = "Attendence";
            this.button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.White;
            this.button8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(319, 102);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(152, 93);
            this.button8.TabIndex = 7;
            this.button8.Text = "Class Schedule";
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // Shiftbutton
            // 
            this.Shiftbutton.BackColor = System.Drawing.Color.White;
            this.Shiftbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Shiftbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Shiftbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Shiftbutton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shiftbutton.Location = new System.Drawing.Point(161, 102);
            this.Shiftbutton.Name = "Shiftbutton";
            this.Shiftbutton.Size = new System.Drawing.Size(152, 93);
            this.Shiftbutton.TabIndex = 6;
            this.Shiftbutton.Text = "Shift";
            this.Shiftbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Shiftbutton.UseVisualStyleBackColor = false;
            this.Shiftbutton.Click += new System.EventHandler(this.Shiftbutton_Click);
            // 
            // Subjectbutton
            // 
            this.Subjectbutton.BackColor = System.Drawing.Color.White;
            this.Subjectbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Subjectbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Subjectbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Subjectbutton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Subjectbutton.Location = new System.Drawing.Point(3, 102);
            this.Subjectbutton.Name = "Subjectbutton";
            this.Subjectbutton.Size = new System.Drawing.Size(152, 93);
            this.Subjectbutton.TabIndex = 5;
            this.Subjectbutton.Text = "Subjects";
            this.Subjectbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Subjectbutton.UseVisualStyleBackColor = false;
            this.Subjectbutton.Click += new System.EventHandler(this.Subjectbutton_Click);
            // 
            // Clsbutton
            // 
            this.Clsbutton.BackColor = System.Drawing.Color.White;
            this.Clsbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Clsbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Clsbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Clsbutton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clsbutton.Location = new System.Drawing.Point(635, 3);
            this.Clsbutton.Name = "Clsbutton";
            this.Clsbutton.Size = new System.Drawing.Size(152, 93);
            this.Clsbutton.TabIndex = 4;
            this.Clsbutton.Text = "Classes";
            this.Clsbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Clsbutton.UseVisualStyleBackColor = false;
            this.Clsbutton.Click += new System.EventHandler(this.Clsbutton_Click);
            // 
            // Sectionbutton
            // 
            this.Sectionbutton.BackColor = System.Drawing.Color.White;
            this.Sectionbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sectionbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Sectionbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Sectionbutton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sectionbutton.Location = new System.Drawing.Point(477, 3);
            this.Sectionbutton.Name = "Sectionbutton";
            this.Sectionbutton.Size = new System.Drawing.Size(152, 93);
            this.Sectionbutton.TabIndex = 3;
            this.Sectionbutton.Text = "Section";
            this.Sectionbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Sectionbutton.UseVisualStyleBackColor = false;
            this.Sectionbutton.Click += new System.EventHandler(this.Sectionbutton_Click);
            // 
            // Admissionbutton
            // 
            this.Admissionbutton.BackColor = System.Drawing.Color.White;
            this.Admissionbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Admissionbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Admissionbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Admissionbutton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Admissionbutton.Location = new System.Drawing.Point(319, 3);
            this.Admissionbutton.Name = "Admissionbutton";
            this.Admissionbutton.Size = new System.Drawing.Size(152, 93);
            this.Admissionbutton.TabIndex = 2;
            this.Admissionbutton.Text = "Admission";
            this.Admissionbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Admissionbutton.UseVisualStyleBackColor = false;
            this.Admissionbutton.Click += new System.EventHandler(this.Admissionbutton_Click);
            // 
            // Rolebutton
            // 
            this.Rolebutton.BackColor = System.Drawing.Color.White;
            this.Rolebutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Rolebutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Rolebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Rolebutton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rolebutton.Location = new System.Drawing.Point(161, 3);
            this.Rolebutton.Name = "Rolebutton";
            this.Rolebutton.Size = new System.Drawing.Size(152, 93);
            this.Rolebutton.TabIndex = 1;
            this.Rolebutton.Text = "Role";
            this.Rolebutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Rolebutton.UseVisualStyleBackColor = false;
            this.Rolebutton.Click += new System.EventHandler(this.Rolebutton_Click);
            // 
            // Staffbutton
            // 
            this.Staffbutton.BackColor = System.Drawing.Color.White;
            this.Staffbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Staffbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Staffbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Staffbutton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Staffbutton.Location = new System.Drawing.Point(3, 3);
            this.Staffbutton.Name = "Staffbutton";
            this.Staffbutton.Size = new System.Drawing.Size(152, 93);
            this.Staffbutton.TabIndex = 0;
            this.Staffbutton.Text = "Staff";
            this.Staffbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Staffbutton.UseVisualStyleBackColor = false;
            this.Staffbutton.Click += new System.EventHandler(this.Staffbutton_Click);
            // 
            // MinimizepictureBox
            // 
            this.MinimizepictureBox.Image = global::SMSS.Properties.Resources.Knob_Remove_icon_minimize_32;
            this.MinimizepictureBox.Location = new System.Drawing.Point(713, 0);
            this.MinimizepictureBox.Name = "MinimizepictureBox";
            this.MinimizepictureBox.Size = new System.Drawing.Size(34, 31);
            this.MinimizepictureBox.TabIndex = 1;
            this.MinimizepictureBox.TabStop = false;
            this.MinimizepictureBox.Click += new System.EventHandler(this.MinimizepictureBox_Click);
            // 
            // ClosepictureBox
            // 
            this.ClosepictureBox.Image = global::SMSS.Properties.Resources.Knob_Cancel_icon_32;
            this.ClosepictureBox.Location = new System.Drawing.Point(753, 0);
            this.ClosepictureBox.Name = "ClosepictureBox";
            this.ClosepictureBox.Size = new System.Drawing.Size(34, 31);
            this.ClosepictureBox.TabIndex = 0;
            this.ClosepictureBox.TabStop = false;
            this.ClosepictureBox.Click += new System.EventHandler(this.ClosepictureBox_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SMSS.Properties.Resources.Picture_of_person;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(41, 39);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(4, 116);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(182, 162);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 498);
            this.ControlBox = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Dashboard";
            this.Text = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.TopPanel.ResumeLayout(false);
            this.CentralPanel.ResumeLayout(false);
            this.TopPanel2.ResumeLayout(false);
            this.LeftPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MinimizepictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosepictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button ClassSchedule;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button Shiftbutton;
        private System.Windows.Forms.Button Subjectbutton;
        private System.Windows.Forms.Button Clsbutton;
        private System.Windows.Forms.Button Sectionbutton;
        private System.Windows.Forms.Button Admissionbutton;
        private System.Windows.Forms.Button Rolebutton;
        private System.Windows.Forms.Button Staffbutton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox MinimizepictureBox;
        private System.Windows.Forms.PictureBox ClosepictureBox;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}