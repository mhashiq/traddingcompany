﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS.Screens
{
    public partial class CSchedule : Layout
    {
        public CSchedule()
        {
            InitializeComponent();
        }

        myDBDataContext context = new myDBDataContext();
        int id;
        int edit = 0;
        private void CSchedule_Load(object sender, EventArgs e)
        {
            loadShiftAndClassesList();
            UtitlityClass.Disable_Reset(TimingCtrlGrp);
        }
        public void LoadeTiming()
        {
            var data = context.Timing_GetTiming();
            TGvId.DataPropertyName = "ID";
            TGvName.DataPropertyName = "Name";
            TGvFrom.DataPropertyName = "From";
            TGvTo.DataPropertyName = "To";
            TGvDays.DataPropertyName = "Days";
            TsGvId.DataPropertyName = "SID";
            TsGvName.DataPropertyName = "SName";
            TcGvId.DataPropertyName = "CID";
            TcGvName.DataPropertyName = "CName";
            TimingdataGridView.DataSource = data;
            UtitlityClass.SNO(TimingdataGridView, "TSno", 0);
        }
        public void loadShiftAndClassesList()
        {
            var data = context.Shift_GetShifts();
            ShiftcomboBox.DataSource = data;
            ShiftcomboBox.DisplayMember = "Name";
            ShiftcomboBox.ValueMember = "ID";

            var data2 = context.Get_Classes();
            ClassescomboBox.DataSource = data2;
            ClassescomboBox.DisplayMember = "Name";
            ClassescomboBox.ValueMember = "ID";
        }
        public override void Addbutton_Click(object sender, EventArgs e)
        {
            edit = 0;
            UtitlityClass.Enable_Reset(TimingCtrlGrp);
        }

        public override void Savebutton_Click(object sender, EventArgs e)
        {
            TimeSpan From = new TimeSpan(FromdateTimePicker.Value.Hour, FromdateTimePicker.Value.Minute, FromdateTimePicker.Value.Second);
            TimeSpan To = new TimeSpan(TodateTimePicker.Value.Hour, TodateTimePicker.Value.Minute, TodateTimePicker.Value.Second);
            if (edit==0)
            { 
                context.Timing_Insert(NametextBox.Text,Convert.ToInt32(ShiftcomboBox.SelectedValue),Convert.ToInt32(ClassescomboBox.SelectedValue),From,To,DayscomboBox.Text);
                UtitlityClass.ShowMsg("Inserted Successfuly!", "Information", "Success");
                UtitlityClass.Disable_Reset(TimingCtrlGrp);
                LoadeTiming();
            }
            else if(edit==1)
            {
                context.Timing_Update(id,NametextBox.Text, Convert.ToInt32(ShiftcomboBox.SelectedValue),Convert.ToInt32(ClassescomboBox.SelectedValue), From, To, DayscomboBox.Text);
                UtitlityClass.ShowMsg("Updated successfuly!", "information", "Success");
                UtitlityClass.Disable_Reset(TimingCtrlGrp);
                LoadeTiming();
            }
        }

        public override void GetRecordbutton_Click(object sender, EventArgs e)
        {
            TimingdataGridView.AutoGenerateColumns = false;
            LoadeTiming();
        }

        public override void Updatebutton_Click(object sender, EventArgs e)
        {
            edit = 1;
            UtitlityClass.Enable(TimingCtrlGrp);
        }

        public override void Delbutton_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Are you sure you want to delete it?", "Conformation!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(r==DialogResult.Yes)
            {
                context.Timing_Delete(id);
                UtitlityClass.Disable_Reset(TimingCtrlGrp);
                LoadeTiming();
            }
            else
            {
                edit = 0;
            }
        }

        public override void Clearbutton_Click(object sender, EventArgs e)
        {

        }

        private void TimingdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex!=-1 && e.ColumnIndex!=-1)
            {
                DataGridViewRow row = TimingdataGridView.Rows[e.RowIndex];
                id = Convert.ToInt32(row.Cells["TGvId"].Value.ToString());
                NametextBox.Text = row.Cells["TGvName"].Value.ToString();
                ShiftcomboBox.SelectedValue = row.Cells["TsGvId"].Value;
                ClassescomboBox.SelectedValue = row.Cells["TcGvId"].Value;
                DayscomboBox.SelectedItem = row.Cells["TGvDays"].Value.ToString();
            }
        }
    }
}
