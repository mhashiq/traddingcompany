﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS.Screens
{
    public partial class StaffForm : Layout
    {
        myDBDataContext context = new myDBDataContext();
        int edit = 0;
        public StaffForm()
        {
            InitializeComponent();
        }
        public void LoadStaffData()
        {
            var data = context.GetStaff();
            SGvID.DataPropertyName = "ST_ID";
            SGvName.DataPropertyName = "Name";
            SGvUN.DataPropertyName = "UserName";
            SGvPW.DataPropertyName = "S_Password";
            SGvPh_1.DataPropertyName = "Phone_1";
            SGvPh_2.DataPropertyName = "Phone_2";
            SGvRole.DataPropertyName = "St_Role";
            SGvSt.DataPropertyName = "St_Status";
            StaffdataGridView.DataSource = data;
            UtitlityClass.SNO(StaffdataGridView, "SGvNo", 0);
        }
        private void StaffForm_Load(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(Controlpanel);
            RolecomboBox.DataSource = context.GetRoles();
            RolecomboBox.DisplayMember = "RoleName";
            RolecomboBox.ValueMember = "Id";
        }
        public override void Addbutton_Click(object sender, EventArgs e)
        {
            edit = 0;
            UtitlityClass.Enable(Controlpanel);
        }
        public override void Savebutton_Click(object sender, EventArgs e)
        {
           if(edit==0)
            {
                if (ImageAddresstextBox.Text == "")
                {
                    var existData = (from x in context.Staffs where x.UserName == UserNametextBox.Text select x).FirstOrDefault();
                    if (existData == null)
                    {
                        context.Staff_InsertWithoutImage(NametextBox.Text, UserNametextBox.Text, PasswordtextBox.Text, Phone1textBox.Text, Phone2textBox.Text, Convert.ToInt32(RolecomboBox.SelectedIndex), Convert.ToByte(StatuscomboBox.SelectedIndex));
                        UtitlityClass.ShowMsg("Add Successfully", "Information", "Success");
                        LoadStaffData();
                    }
                    else
                    {
                        UtitlityClass.ShowMsg($"{UserNametextBox.Text} is available!", "informatio", "Error");
                    }
                }
                else
                {
                    var existData = (from x in context.Staffs where x.UserName == UserNametextBox.Text select x).FirstOrDefault();
                    if (existData == null)
                    {
                        MemoryStream ms = new MemoryStream();
                        i.Save(ms, ImageFormat.Jpeg);
                        byte[] arr = ms.ToArray();
                        context.Staff_Insert(NametextBox.Text, UserNametextBox.Text, PasswordtextBox.Text, Phone1textBox.Text, Phone2textBox.Text, Convert.ToInt32(RolecomboBox.SelectedIndex), Convert.ToByte(StatuscomboBox.SelectedIndex), arr);
                        UtitlityClass.ShowMsg("Add Successfully", "Information", "Success");
                        LoadStaffData();
                    }
                    else
                    {
                        UtitlityClass.ShowMsg($"{UserNametextBox.Text} is available!", "informatio", "Error");
                    }
                }
            }
           else if(edit==1)
            {
                var data = context.Staffs.Single(x=>x.ST_ID==sid);
                data.Name = NametextBox.Text;
                data.UserName = UserNametextBox.Text;
                data.S_Password = PasswordtextBox.Text;
                data.Phone_1 = Phone1textBox.Text;
                data.Phone_2 = Phone2textBox.Text;
                data.St_Role = Convert.ToInt32(RolecomboBox.SelectedIndex);
                data.St_Status = Convert.ToByte(StatuscomboBox.SelectedIndex);
                MemoryStream ms = new MemoryStream();
                i.Save(ms, ImageFormat.Jpeg);
                byte[] arr = ms.ToArray();
                data.St_Image = arr;
                context.SubmitChanges();
                UtitlityClass.ShowMsg("Updated Successfuly!", "Information", "Success");
            }
        }

        public override void GetRecordbutton_Click(object sender, EventArgs e)
        {
            LoadStaffData();
        }

        public override void Updatebutton_Click(object sender, EventArgs e)
        {
            edit = 1;
            UtitlityClass.Enable(Controlpanel); 
        }

        public override void Delbutton_Click(object sender, EventArgs e)
        {

        }

        public override void Clearbutton_Click(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(Controlpanel);
            edit = 0;
        }
        Image i;
        private void PicAddbutton_Click(object sender, EventArgs e)
        {
            DialogResult dr = StaffPicopenFileDialog.ShowDialog();
            if(dr==DialogResult.OK)
            {
                i = new Bitmap(StaffPicopenFileDialog.FileName);
                StaffpictureBox.Image = i;
                ImageAddresstextBox.Text = StaffPicopenFileDialog.FileName;
            }
        }
        int sid;
        private void StaffdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex!=1 && e.ColumnIndex!=1)
            {
                DataGridViewRow row = StaffdataGridView.Rows[e.RowIndex];
                sid = Convert.ToInt32(row.Cells["SGvID"].Value.ToString());
                NametextBox.Text = row.Cells["SGvName"].Value.ToString();
                UserNametextBox.Text = row.Cells["SGvUN"].Value.ToString();
                PasswordtextBox.Text = row.Cells["SGvPW"].Value.ToString();
                Phone1textBox.Text = row.Cells["SGvPh_1"].Value.ToString();
                Phone2textBox.Text = row.Cells["SGvPh_2"].Value.ToString();
                RolecomboBox.SelectedItem = row.Cells["SGvRole"].Value.ToString();
                StatuscomboBox.SelectedItem = row.Cells["SGvSt"].Value.ToString();
                var image = (from x in context.Staffs where x.ST_ID == sid select x.St_Image).First();
                if(image!=null)
                {
                    byte[] arr = image.ToArray();
                    MemoryStream ms = new MemoryStream(arr);
                    i = Image.FromStream(ms);
                    StaffpictureBox.Image = i;
                }
                else
                {
                    StaffpictureBox.Image = null;
                }
                UtitlityClass.Disable(Controlpanel);
            }
        }
    }

  
}
