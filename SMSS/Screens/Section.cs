﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS.Screens
{
    public partial class Section : Layout,IDisposable
    {
        int edit = 0;
        int id;
        myDBDataContext context = new myDBDataContext();
        public Section()
        {
            InitializeComponent();
        }
        public void LoadData()
        {
            var data = context.Sec_GetSection();
            SGvID.DataPropertyName = "ID";
            SGvName.DataPropertyName = "Name";
            SGvSt.DataPropertyName = "Status";
            SectiondataGridView.DataSource = data;
            UtitlityClass.SNO(SectiondataGridView, "SGvNo", 0);
        }
        private void Section_Load(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(SectiongroupBox);
        }
        public override void Addbutton_Click(object sender, EventArgs e)
        {
            edit = 0;
            UtitlityClass.Enable(SectiongroupBox);
        }

        public override void Savebutton_Click(object sender, EventArgs e)
        {
            if(edit==0)
            {
                    byte stat = StatuscomboBox.SelectedIndex == 0 ? Convert.ToByte(1) : Convert.ToByte(0);
                    context.Sec_Insert(SectiontextBox.Text,stat);
                    UtitlityClass.ShowMsg("Successfuly added!", "Information", "Success");
                    LoadData();
            }
            else if(edit==1)
            {
                var data = context.sections.Single(x => x.Sec_Id == id);
                context.Sec_Update(id, SectiontextBox.Text, StatuscomboBox.SelectedIndex == 0 ? Convert.ToByte(1) : Convert.ToByte(0));
                UtitlityClass.ShowMsg("Updated Successfully", "Information", "Success");
                LoadData();
            }
        }

        public override void GetRecordbutton_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        public override void Updatebutton_Click(object sender, EventArgs e)
        {
            edit = 1;
            UtitlityClass.Enable(SectiongroupBox);
        }

        public override void Delbutton_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete it?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(dr==DialogResult.Yes)
            {
                context.Sec_Delete(id);
                UtitlityClass.ShowMsg("Deleted Successfully!","Information","Success");
                UtitlityClass.Disable_Reset(SectiongroupBox);
                LoadData();
            }
        }

        public override void Clearbutton_Click(object sender, EventArgs e)
        {
            edit = 0;
            UtitlityClass.Disable_Reset(SectiongroupBox);
        }

        private void SectiondataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex!=-1 && e.ColumnIndex!=-1)
            {
                DataGridViewRow row = SectiondataGridView.Rows[e.RowIndex];
                id = Convert.ToInt32(row.Cells["SGvID"].Value.ToString());
                SectiontextBox.Text = row.Cells["SGvName"].Value.ToString();
                byte stat = Convert.ToByte(row.Cells["SGvSt"].Value.ToString());
                StatuscomboBox.SelectedItem = stat == 1 ? "Active" : "In-Active";
            }
        }
    }
}
