﻿namespace SMSS.Screens
{
    partial class Section
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SectiongroupBox = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.StatuscomboBox = new System.Windows.Forms.ComboBox();
            this.SectiontextBox = new System.Windows.Forms.TextBox();
            this.GridViewpanel = new System.Windows.Forms.Panel();
            this.SectiondataGridView = new System.Windows.Forms.DataGridView();
            this.SGvNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvSt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Centerpanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SectiongroupBox.SuspendLayout();
            this.GridViewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SectiondataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Toppanel
            // 
            this.Toppanel.Size = new System.Drawing.Size(552, 76);
            // 
            // ButtonControlpanel
            // 
            this.ButtonControlpanel.Size = new System.Drawing.Size(552, 53);
            // 
            // Centerpanel
            // 
            this.Centerpanel.Controls.Add(this.GridViewpanel);
            this.Centerpanel.Controls.Add(this.panel1);
            this.Centerpanel.Size = new System.Drawing.Size(552, 223);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.SectiongroupBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(552, 100);
            this.panel1.TabIndex = 0;
            // 
            // SectiongroupBox
            // 
            this.SectiongroupBox.Controls.Add(this.label2);
            this.SectiongroupBox.Controls.Add(this.label1);
            this.SectiongroupBox.Controls.Add(this.StatuscomboBox);
            this.SectiongroupBox.Controls.Add(this.SectiontextBox);
            this.SectiongroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SectiongroupBox.Location = new System.Drawing.Point(0, 0);
            this.SectiongroupBox.Name = "SectiongroupBox";
            this.SectiongroupBox.Size = new System.Drawing.Size(552, 100);
            this.SectiongroupBox.TabIndex = 0;
            this.SectiongroupBox.TabStop = false;
            this.SectiongroupBox.Text = "Section Entry";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Status:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Section:";
            // 
            // StatuscomboBox
            // 
            this.StatuscomboBox.FormattingEnabled = true;
            this.StatuscomboBox.Items.AddRange(new object[] {
            "Active",
            "In-Active"});
            this.StatuscomboBox.Location = new System.Drawing.Point(126, 63);
            this.StatuscomboBox.Name = "StatuscomboBox";
            this.StatuscomboBox.Size = new System.Drawing.Size(344, 21);
            this.StatuscomboBox.TabIndex = 1;
            // 
            // SectiontextBox
            // 
            this.SectiontextBox.Location = new System.Drawing.Point(126, 29);
            this.SectiontextBox.Name = "SectiontextBox";
            this.SectiontextBox.Size = new System.Drawing.Size(344, 20);
            this.SectiontextBox.TabIndex = 0;
            // 
            // GridViewpanel
            // 
            this.GridViewpanel.Controls.Add(this.SectiondataGridView);
            this.GridViewpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridViewpanel.Location = new System.Drawing.Point(0, 100);
            this.GridViewpanel.Name = "GridViewpanel";
            this.GridViewpanel.Size = new System.Drawing.Size(552, 123);
            this.GridViewpanel.TabIndex = 1;
            // 
            // SectiondataGridView
            // 
            this.SectiondataGridView.AllowUserToAddRows = false;
            this.SectiondataGridView.AllowUserToDeleteRows = false;
            this.SectiondataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SectiondataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.SectiondataGridView.BackgroundColor = System.Drawing.Color.White;
            this.SectiondataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SectiondataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SGvNo,
            this.SGvID,
            this.SGvName,
            this.SGvSt});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SectiondataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.SectiondataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SectiondataGridView.Location = new System.Drawing.Point(0, 0);
            this.SectiondataGridView.Name = "SectiondataGridView";
            this.SectiondataGridView.ReadOnly = true;
            this.SectiondataGridView.RowHeadersVisible = false;
            this.SectiondataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SectiondataGridView.Size = new System.Drawing.Size(552, 123);
            this.SectiondataGridView.TabIndex = 0;
            this.SectiondataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SectiondataGridView_CellClick);
            // 
            // SGvNo
            // 
            this.SGvNo.HeaderText = "Sno";
            this.SGvNo.Name = "SGvNo";
            this.SGvNo.ReadOnly = true;
            // 
            // SGvID
            // 
            this.SGvID.HeaderText = "ID";
            this.SGvID.Name = "SGvID";
            this.SGvID.ReadOnly = true;
            this.SGvID.Visible = false;
            // 
            // SGvName
            // 
            this.SGvName.HeaderText = "Section Name";
            this.SGvName.Name = "SGvName";
            this.SGvName.ReadOnly = true;
            // 
            // SGvSt
            // 
            this.SGvSt.HeaderText = "Status";
            this.SGvSt.Name = "SGvSt";
            this.SGvSt.ReadOnly = true;
            // 
            // Section
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 352);
            this.MaximizeBox = false;
            this.Name = "Section";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Section";
            this.Load += new System.EventHandler(this.Section_Load);
            this.Centerpanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.SectiongroupBox.ResumeLayout(false);
            this.SectiongroupBox.PerformLayout();
            this.GridViewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SectiondataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox SectiongroupBox;
        private System.Windows.Forms.ComboBox StatuscomboBox;
        private System.Windows.Forms.TextBox SectiontextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel GridViewpanel;
        private System.Windows.Forms.DataGridView SectiondataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvSt;
    }
}