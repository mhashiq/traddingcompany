﻿namespace SMSS.Screens
{
    partial class Subjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Sujectpanel = new System.Windows.Forms.Panel();
            this.SubjectgroupBox = new System.Windows.Forms.GroupBox();
            this.ClasscomboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.StatuscomboBox = new System.Windows.Forms.ComboBox();
            this.SubjectNametextBox = new System.Windows.Forms.TextBox();
            this.GridViewpanel = new System.Windows.Forms.Panel();
            this.SubjectdataGridView = new System.Windows.Forms.DataGridView();
            this.SuGvSno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SuCGvId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SuGvId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SuGvName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SuGvClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SuGvSt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Centerpanel.SuspendLayout();
            this.Sujectpanel.SuspendLayout();
            this.SubjectgroupBox.SuspendLayout();
            this.GridViewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SubjectdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Toppanel
            // 
            this.Toppanel.Size = new System.Drawing.Size(684, 76);
            // 
            // ButtonControlpanel
            // 
            this.ButtonControlpanel.Size = new System.Drawing.Size(684, 53);
            // 
            // Centerpanel
            // 
            this.Centerpanel.Controls.Add(this.GridViewpanel);
            this.Centerpanel.Controls.Add(this.Sujectpanel);
            this.Centerpanel.Size = new System.Drawing.Size(684, 282);
            // 
            // Sujectpanel
            // 
            this.Sujectpanel.Controls.Add(this.SubjectgroupBox);
            this.Sujectpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Sujectpanel.Location = new System.Drawing.Point(0, 0);
            this.Sujectpanel.Name = "Sujectpanel";
            this.Sujectpanel.Size = new System.Drawing.Size(684, 128);
            this.Sujectpanel.TabIndex = 0;
            // 
            // SubjectgroupBox
            // 
            this.SubjectgroupBox.Controls.Add(this.ClasscomboBox);
            this.SubjectgroupBox.Controls.Add(this.label2);
            this.SubjectgroupBox.Controls.Add(this.label3);
            this.SubjectgroupBox.Controls.Add(this.label1);
            this.SubjectgroupBox.Controls.Add(this.StatuscomboBox);
            this.SubjectgroupBox.Controls.Add(this.SubjectNametextBox);
            this.SubjectgroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SubjectgroupBox.Location = new System.Drawing.Point(0, 0);
            this.SubjectgroupBox.Name = "SubjectgroupBox";
            this.SubjectgroupBox.Size = new System.Drawing.Size(684, 128);
            this.SubjectgroupBox.TabIndex = 0;
            this.SubjectgroupBox.TabStop = false;
            this.SubjectgroupBox.Text = "Subject Entry";
            // 
            // ClasscomboBox
            // 
            this.ClasscomboBox.FormattingEnabled = true;
            this.ClasscomboBox.Location = new System.Drawing.Point(173, 59);
            this.ClasscomboBox.Name = "ClasscomboBox";
            this.ClasscomboBox.Size = new System.Drawing.Size(344, 21);
            this.ClasscomboBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(117, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Status:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(117, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Class:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(111, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Subject:";
            // 
            // StatuscomboBox
            // 
            this.StatuscomboBox.FormattingEnabled = true;
            this.StatuscomboBox.Items.AddRange(new object[] {
            "Active",
            "In-Active"});
            this.StatuscomboBox.Location = new System.Drawing.Point(173, 95);
            this.StatuscomboBox.Name = "StatuscomboBox";
            this.StatuscomboBox.Size = new System.Drawing.Size(344, 21);
            this.StatuscomboBox.TabIndex = 1;
            // 
            // SubjectNametextBox
            // 
            this.SubjectNametextBox.Location = new System.Drawing.Point(172, 23);
            this.SubjectNametextBox.Name = "SubjectNametextBox";
            this.SubjectNametextBox.Size = new System.Drawing.Size(344, 20);
            this.SubjectNametextBox.TabIndex = 0;
            // 
            // GridViewpanel
            // 
            this.GridViewpanel.Controls.Add(this.SubjectdataGridView);
            this.GridViewpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridViewpanel.Location = new System.Drawing.Point(0, 128);
            this.GridViewpanel.Name = "GridViewpanel";
            this.GridViewpanel.Size = new System.Drawing.Size(684, 154);
            this.GridViewpanel.TabIndex = 1;
            // 
            // SubjectdataGridView
            // 
            this.SubjectdataGridView.AllowUserToAddRows = false;
            this.SubjectdataGridView.AllowUserToDeleteRows = false;
            this.SubjectdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SubjectdataGridView.BackgroundColor = System.Drawing.Color.White;
            this.SubjectdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SubjectdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SuGvSno,
            this.SuCGvId,
            this.SuGvId,
            this.SuGvName,
            this.SuGvClass,
            this.SuGvSt});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SubjectdataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.SubjectdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SubjectdataGridView.Location = new System.Drawing.Point(0, 0);
            this.SubjectdataGridView.MultiSelect = false;
            this.SubjectdataGridView.Name = "SubjectdataGridView";
            this.SubjectdataGridView.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SubjectdataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.SubjectdataGridView.RowHeadersVisible = false;
            this.SubjectdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SubjectdataGridView.Size = new System.Drawing.Size(684, 154);
            this.SubjectdataGridView.TabIndex = 0;
            this.SubjectdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SubjectdataGridView_CellClick);
            // 
            // SuGvSno
            // 
            this.SuGvSno.HeaderText = "Sno";
            this.SuGvSno.Name = "SuGvSno";
            this.SuGvSno.ReadOnly = true;
            // 
            // SuCGvId
            // 
            this.SuCGvId.HeaderText = "Class Id";
            this.SuCGvId.Name = "SuCGvId";
            this.SuCGvId.ReadOnly = true;
            this.SuCGvId.Visible = false;
            // 
            // SuGvId
            // 
            this.SuGvId.HeaderText = "ID";
            this.SuGvId.Name = "SuGvId";
            this.SuGvId.ReadOnly = true;
            this.SuGvId.Visible = false;
            // 
            // SuGvName
            // 
            this.SuGvName.HeaderText = "Subject Name";
            this.SuGvName.Name = "SuGvName";
            this.SuGvName.ReadOnly = true;
            // 
            // SuGvClass
            // 
            this.SuGvClass.HeaderText = "Class";
            this.SuGvClass.Name = "SuGvClass";
            this.SuGvClass.ReadOnly = true;
            // 
            // SuGvSt
            // 
            this.SuGvSt.HeaderText = "Status";
            this.SuGvSt.Name = "SuGvSt";
            this.SuGvSt.ReadOnly = true;
            // 
            // Subjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.MaximizeBox = false;
            this.Name = "Subjects";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Subjects";
            this.Load += new System.EventHandler(this.Subjects_Load);
            this.Centerpanel.ResumeLayout(false);
            this.Sujectpanel.ResumeLayout(false);
            this.SubjectgroupBox.ResumeLayout(false);
            this.SubjectgroupBox.PerformLayout();
            this.GridViewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SubjectdataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel GridViewpanel;
        private System.Windows.Forms.DataGridView SubjectdataGridView;
        private System.Windows.Forms.Panel Sujectpanel;
        private System.Windows.Forms.GroupBox SubjectgroupBox;
        private System.Windows.Forms.ComboBox StatuscomboBox;
        private System.Windows.Forms.TextBox SubjectNametextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ClasscomboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn SuGvSno;
        private System.Windows.Forms.DataGridViewTextBoxColumn SuCGvId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SuGvId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SuGvName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SuGvClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn SuGvSt;
    }
}