﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS.Screens
{
    public partial class Roles : Layout
    {
        public Roles()
        {
            InitializeComponent();
        }
        myDBDataContext db = new myDBDataContext();
        int edit=0;
        private void Roles_Load(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(TextControlgroupBox);
        }
        public override void Addbutton_Click(object sender, EventArgs e)
        {
            edit = 0;
            UtitlityClass.Enable(TextControlgroupBox);
           // MessageBox.Show("Asslam o alaikum! its Add New Button","Info",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        public override void Savebutton_Click(object sender, EventArgs e)
        {
           
            Role r = new Role();
            //edit = 0;
            if(edit==0)
            {
                r.RoleName = RNametextBox.Text;
                r.RoleStatus = StatuscomboBox.Text;
                db.Roles.InsertOnSubmit(r);
                db.SubmitChanges();
                DataGrid();
                MessageBox.Show("Insert Data Successfully", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           else if(edit==1)
            {
                var data = db.Roles.Single(x => x.Id == gridId);
                data.RoleName = RNametextBox.Text;
                data.RoleStatus = StatuscomboBox.Text;
                db.SubmitChanges();
                DataGrid();
                MessageBox.Show("Update Data Successfully", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        public void DataGrid()
        {
            var data = db.GetRoles();
            rolesIDGV.DataPropertyName = "Id";
            rolesNameGV.DataPropertyName = "RoleName";
            statusGV.DataPropertyName = "RoleStatus";
            RoledataGridView.DataSource = data;
            UtitlityClass.SNO(RoledataGridView, "snoGV",0);
        }
        public override void GetRecordbutton_Click(object sender, EventArgs e)
        {
            DataGrid();
        }

        public override void Updatebutton_Click(object sender, EventArgs e)
        {
            edit = 1;
            UtitlityClass.Enable(TextControlgroupBox);
        }

        public override void Delbutton_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure!you want to delete " + RNametextBox.Text + "?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(dr==DialogResult.Yes)
            {
                db.RoleDelete(gridId);
                DataGrid();
                MessageBox.Show("Deleted Successfully!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        public override void Clearbutton_Click(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(TextControlgroupBox);
           // MessageBox.Show("Asslam o alaikum! its clear Button", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        int gridId;
        private void RoledataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex!=-1 && e.ColumnIndex!=-1)
            {
                DataGridViewRow row = RoledataGridView.Rows[e.RowIndex];
                gridId = Convert.ToInt32(row.Cells["rolesIDGV"].Value.ToString());
                RNametextBox.Text = row.Cells["rolesNameGV"].Value.ToString();
                StatuscomboBox.Text = row.Cells["statusGV"].Value.ToString();
                UtitlityClass.Disable(TextControlgroupBox);
            }
        }
    }
}
