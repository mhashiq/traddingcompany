﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS.Screens
{
    public partial class Subjects : Layout
    {
        int edit = 0;
        myDBDataContext context = new myDBDataContext();
        int id;
        public Subjects()
        {
            InitializeComponent();
        }

        private void Subjects_Load(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(SubjectgroupBox);
            LoadList();
        }
        public void LoadList()
        {
            ClasscomboBox.DataSource = context.Get_Classes();
            ClasscomboBox.DisplayMember = "Name";
            ClasscomboBox.ValueMember = "ID";
        }
        public void LoadData()
        {
            var data = context.Sub_GetSubjects();
            SuGvId.DataPropertyName = "ID";
            SuGvName.DataPropertyName = "Name";
            SuGvClass.DataPropertyName = "CName";
            SuCGvId.DataPropertyName = "ClassID";
            SuGvSt.DataPropertyName = "Status";
            SubjectdataGridView.DataSource = data;
            UtitlityClass.SNO(SubjectdataGridView, "SuGvSno", 0);
        }
        public override void Addbutton_Click(object sender, EventArgs e)
        {
            edit = 0;
            UtitlityClass.Enable_Reset(SubjectgroupBox);
        }

        public override void Savebutton_Click(object sender, EventArgs e)
        {
            if (edit == 0)
            {
                context.Sub_Insert(Convert.ToInt32(ClasscomboBox.SelectedValue),SubjectNametextBox.Text,StatuscomboBox.SelectedIndex == 0 ? Convert.ToByte(1) : Convert.ToByte(0));
                UtitlityClass.ShowMsg("Insert Successfuly!", "Information", "Success");
                LoadData();
                UtitlityClass.Disable_Reset(SubjectgroupBox);
            }
            else if (edit == 1)
            {
                context.Sub_Update(Convert.ToInt32(ClasscomboBox.SelectedValue),id, SubjectNametextBox.Text, StatuscomboBox.SelectedIndex == 0 ? Convert.ToByte(1) : Convert.ToByte(0));
                UtitlityClass.ShowMsg("Update Successfuly!", "Information", "Success");
                LoadData();
                UtitlityClass.Disable_Reset(SubjectgroupBox);
            }
        }

        public override void GetRecordbutton_Click(object sender, EventArgs e)
        {
            SubjectdataGridView.AutoGenerateColumns = false;
            LoadData();
        }

        public override void Updatebutton_Click(object sender, EventArgs e)
        {
            edit = 1;
            UtitlityClass.Enable(SubjectgroupBox);
        }

        public override void Delbutton_Click(object sender, EventArgs e)
        {
            context.Sub_Delete(id);
            UtitlityClass.ShowMsg("Deleted Successfuly!", "Information", "Success");
            LoadData();
            UtitlityClass.Disable_Reset(SubjectgroupBox);
        }

        public override void Clearbutton_Click(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(SubjectgroupBox);
        }

        private void SubjectdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                UtitlityClass.Disable(SubjectgroupBox);
                DataGridViewRow row = SubjectdataGridView.Rows[e.RowIndex];
                id = Convert.ToInt32(row.Cells["SuGvId"].Value.ToString());
                SubjectNametextBox.Text = row.Cells["SuGvName"].Value.ToString();
                StatuscomboBox.SelectedItem = Convert.ToByte(row.Cells["SuGvSt"].Value.ToString()) == 1 ? "Active" : "In-Active";
                ClasscomboBox.SelectedItem = row.Cells["SuGvClass"].Value.ToString();
            }
        }
    }
}
