﻿namespace SMSS.Screens
{
    partial class Standerd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Standerdpanel = new System.Windows.Forms.Panel();
            this.StanderdgroupBox = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.StatuscomboBox = new System.Windows.Forms.ComboBox();
            this.StanderdNametextBox = new System.Windows.Forms.TextBox();
            this.GridViewpanel = new System.Windows.Forms.Panel();
            this.StanderddataGridView = new System.Windows.Forms.DataGridView();
            this.CGvSno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGvId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGvName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGvSt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Centerpanel.SuspendLayout();
            this.Standerdpanel.SuspendLayout();
            this.StanderdgroupBox.SuspendLayout();
            this.GridViewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StanderddataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Toppanel
            // 
            this.Toppanel.Size = new System.Drawing.Size(552, 76);
            // 
            // ButtonControlpanel
            // 
            this.ButtonControlpanel.Size = new System.Drawing.Size(552, 53);
            // 
            // Centerpanel
            // 
            this.Centerpanel.Controls.Add(this.GridViewpanel);
            this.Centerpanel.Controls.Add(this.Standerdpanel);
            this.Centerpanel.Size = new System.Drawing.Size(552, 223);
            // 
            // Standerdpanel
            // 
            this.Standerdpanel.Controls.Add(this.StanderdgroupBox);
            this.Standerdpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Standerdpanel.Location = new System.Drawing.Point(0, 0);
            this.Standerdpanel.Name = "Standerdpanel";
            this.Standerdpanel.Size = new System.Drawing.Size(552, 100);
            this.Standerdpanel.TabIndex = 0;
            // 
            // StanderdgroupBox
            // 
            this.StanderdgroupBox.Controls.Add(this.label2);
            this.StanderdgroupBox.Controls.Add(this.label1);
            this.StanderdgroupBox.Controls.Add(this.StatuscomboBox);
            this.StanderdgroupBox.Controls.Add(this.StanderdNametextBox);
            this.StanderdgroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StanderdgroupBox.Location = new System.Drawing.Point(0, 0);
            this.StanderdgroupBox.Name = "StanderdgroupBox";
            this.StanderdgroupBox.Size = new System.Drawing.Size(552, 100);
            this.StanderdgroupBox.TabIndex = 0;
            this.StanderdgroupBox.TabStop = false;
            this.StanderdgroupBox.Text = "Standerd Entry";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Status:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Class Name:";
            // 
            // StatuscomboBox
            // 
            this.StatuscomboBox.FormattingEnabled = true;
            this.StatuscomboBox.Items.AddRange(new object[] {
            "Active",
            "In-Active"});
            this.StatuscomboBox.Location = new System.Drawing.Point(115, 66);
            this.StatuscomboBox.Name = "StatuscomboBox";
            this.StatuscomboBox.Size = new System.Drawing.Size(344, 21);
            this.StatuscomboBox.TabIndex = 1;
            // 
            // StanderdNametextBox
            // 
            this.StanderdNametextBox.Location = new System.Drawing.Point(115, 30);
            this.StanderdNametextBox.Name = "StanderdNametextBox";
            this.StanderdNametextBox.Size = new System.Drawing.Size(344, 20);
            this.StanderdNametextBox.TabIndex = 0;
            // 
            // GridViewpanel
            // 
            this.GridViewpanel.Controls.Add(this.StanderddataGridView);
            this.GridViewpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridViewpanel.Location = new System.Drawing.Point(0, 100);
            this.GridViewpanel.Name = "GridViewpanel";
            this.GridViewpanel.Size = new System.Drawing.Size(552, 123);
            this.GridViewpanel.TabIndex = 1;
            // 
            // StanderddataGridView
            // 
            this.StanderddataGridView.AllowUserToAddRows = false;
            this.StanderddataGridView.AllowUserToDeleteRows = false;
            this.StanderddataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.StanderddataGridView.BackgroundColor = System.Drawing.Color.White;
            this.StanderddataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StanderddataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CGvSno,
            this.CGvId,
            this.CGvName,
            this.CGvSt});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.StanderddataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.StanderddataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StanderddataGridView.Location = new System.Drawing.Point(0, 0);
            this.StanderddataGridView.Name = "StanderddataGridView";
            this.StanderddataGridView.ReadOnly = true;
            this.StanderddataGridView.RowHeadersVisible = false;
            this.StanderddataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.StanderddataGridView.Size = new System.Drawing.Size(552, 123);
            this.StanderddataGridView.TabIndex = 0;
            this.StanderddataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.StanderddataGridView_CellClick);
            // 
            // CGvSno
            // 
            this.CGvSno.HeaderText = "Sno";
            this.CGvSno.Name = "CGvSno";
            this.CGvSno.ReadOnly = true;
            // 
            // CGvId
            // 
            this.CGvId.HeaderText = "ID";
            this.CGvId.Name = "CGvId";
            this.CGvId.ReadOnly = true;
            this.CGvId.Visible = false;
            // 
            // CGvName
            // 
            this.CGvName.HeaderText = "Class Name";
            this.CGvName.Name = "CGvName";
            this.CGvName.ReadOnly = true;
            // 
            // CGvSt
            // 
            this.CGvSt.HeaderText = "Status";
            this.CGvSt.Name = "CGvSt";
            this.CGvSt.ReadOnly = true;
            // 
            // Standerd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 352);
            this.MaximizeBox = false;
            this.Name = "Standerd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Standerd";
            this.Load += new System.EventHandler(this.Standerd_Load);
            this.Centerpanel.ResumeLayout(false);
            this.Standerdpanel.ResumeLayout(false);
            this.StanderdgroupBox.ResumeLayout(false);
            this.StanderdgroupBox.PerformLayout();
            this.GridViewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StanderddataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Standerdpanel;
        private System.Windows.Forms.GroupBox StanderdgroupBox;
        private System.Windows.Forms.ComboBox StatuscomboBox;
        private System.Windows.Forms.TextBox StanderdNametextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel GridViewpanel;
        private System.Windows.Forms.DataGridView StanderddataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGvSno;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGvId;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGvName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGvSt;
    }
}