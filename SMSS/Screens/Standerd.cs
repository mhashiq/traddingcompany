﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS.Screens
{
    public partial class Standerd : Layout
    {
        int edit = 0;
        myDBDataContext context = new myDBDataContext();
        int id;
        public Standerd()
        {
            InitializeComponent();
        }
        public void LoadData()
        {
            var data = context.Get_Classes();
            CGvId.DataPropertyName = "ID";
            CGvName.DataPropertyName = "Name";
            CGvSt.DataPropertyName = "Status";
            StanderddataGridView.DataSource = data;
            UtitlityClass.SNO(StanderddataGridView, "CGvSno", 0);
        }
        private void Standerd_Load(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(StanderdgroupBox);
        }
        public override void Addbutton_Click(object sender, EventArgs e)
        {
            edit = 0;
            UtitlityClass.Enable(StanderdgroupBox);
        }

        public override void Savebutton_Click(object sender, EventArgs e)
        {
            if(edit==0)
            {
                context.Class_Insert(StanderdNametextBox.Text, StatuscomboBox.SelectedIndex == 0 ? Convert.ToByte(1) : Convert.ToByte(0));
                UtitlityClass.ShowMsg("Insert Successfuly!", "Information", "Success");
                LoadData();
                UtitlityClass.Disable_Reset(StanderdgroupBox);
            }
            else if(edit==1)
            {
                context.Class_Update(id,StanderdNametextBox.Text, StatuscomboBox.SelectedIndex == 0 ? Convert.ToByte(1) : Convert.ToByte(0));
                UtitlityClass.ShowMsg("Update Successfuly!", "Information", "Success");
                LoadData();
                UtitlityClass.Disable_Reset(StanderdgroupBox);
            }
        }

        public override void GetRecordbutton_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        public override void Updatebutton_Click(object sender, EventArgs e)
        {
            edit = 1;
            UtitlityClass.Enable(StanderdgroupBox);
        }

        public override void Delbutton_Click(object sender, EventArgs e)
        {
            context.Class_Delete(id);
            UtitlityClass.ShowMsg("Deleted Successfuly!", "Information", "Success");
            LoadData();
            UtitlityClass.Disable_Reset(StanderdgroupBox);
        }

        public override void Clearbutton_Click(object sender, EventArgs e)
        {
            UtitlityClass.Disable_Reset(StanderdgroupBox);
        }

        private void StanderddataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex!=-1 && e.ColumnIndex!=-1)
            {
                UtitlityClass.Disable(StanderdgroupBox);
                DataGridViewRow row = StanderddataGridView.Rows[e.RowIndex];
                id = Convert.ToInt32(row.Cells["CGvId"].Value.ToString());
                StanderdNametextBox.Text = row.Cells["CGvName"].Value.ToString();
                StatuscomboBox.SelectedItem = Convert.ToByte(row.Cells["CGvSt"].Value.ToString()) == 1 ? "Active" : "In-Active";
            }
        }
    }
}
