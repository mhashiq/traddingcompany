﻿namespace SMSS.Screens
{
    partial class StaffForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StaffForm));
            this.Controlpanel = new System.Windows.Forms.Panel();
            this.ImageAddresstextBox = new System.Windows.Forms.TextBox();
            this.PicAddbutton = new System.Windows.Forms.Button();
            this.StaffpictureBox = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.StatuscomboBox = new System.Windows.Forms.ComboBox();
            this.RolecomboBox = new System.Windows.Forms.ComboBox();
            this.Phone2textBox = new System.Windows.Forms.TextBox();
            this.Phone1textBox = new System.Windows.Forms.TextBox();
            this.PasswordtextBox = new System.Windows.Forms.TextBox();
            this.UserNametextBox = new System.Windows.Forms.TextBox();
            this.NametextBox = new System.Windows.Forms.TextBox();
            this.Operationpanel = new System.Windows.Forms.Panel();
            this.SearchgroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.SByNmradioButton = new System.Windows.Forms.RadioButton();
            this.SByUNradioButton = new System.Windows.Forms.RadioButton();
            this.SByPhradioButton = new System.Windows.Forms.RadioButton();
            this.SearchTextgroupBox = new System.Windows.Forms.GroupBox();
            this.SearchtextBox = new System.Windows.Forms.TextBox();
            this.GridViewpanel = new System.Windows.Forms.Panel();
            this.StaffdataGridView = new System.Windows.Forms.DataGridView();
            this.SGvNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvPW = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvPh_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvPh_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvRole = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGvSt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.StaffPicopenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.Toppanel.SuspendLayout();
            this.Centerpanel.SuspendLayout();
            this.Controlpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StaffpictureBox)).BeginInit();
            this.Operationpanel.SuspendLayout();
            this.SearchgroupBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SearchTextgroupBox.SuspendLayout();
            this.GridViewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StaffdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Toppanel
            // 
            this.Toppanel.Controls.Add(this.label9);
            this.Toppanel.Controls.Add(this.label8);
            this.Toppanel.Controls.Add(this.pictureBox1);
            this.Toppanel.Size = new System.Drawing.Size(1024, 76);
            // 
            // ButtonControlpanel
            // 
            this.ButtonControlpanel.Size = new System.Drawing.Size(1024, 53);
            // 
            // Centerpanel
            // 
            this.Centerpanel.Controls.Add(this.GridViewpanel);
            this.Centerpanel.Controls.Add(this.Operationpanel);
            this.Centerpanel.Controls.Add(this.Controlpanel);
            this.Centerpanel.Size = new System.Drawing.Size(1024, 482);
            // 
            // Controlpanel
            // 
            this.Controlpanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controlpanel.Controls.Add(this.ImageAddresstextBox);
            this.Controlpanel.Controls.Add(this.PicAddbutton);
            this.Controlpanel.Controls.Add(this.StaffpictureBox);
            this.Controlpanel.Controls.Add(this.label3);
            this.Controlpanel.Controls.Add(this.label7);
            this.Controlpanel.Controls.Add(this.label6);
            this.Controlpanel.Controls.Add(this.label5);
            this.Controlpanel.Controls.Add(this.label4);
            this.Controlpanel.Controls.Add(this.label2);
            this.Controlpanel.Controls.Add(this.label1);
            this.Controlpanel.Controls.Add(this.StatuscomboBox);
            this.Controlpanel.Controls.Add(this.RolecomboBox);
            this.Controlpanel.Controls.Add(this.Phone2textBox);
            this.Controlpanel.Controls.Add(this.Phone1textBox);
            this.Controlpanel.Controls.Add(this.PasswordtextBox);
            this.Controlpanel.Controls.Add(this.UserNametextBox);
            this.Controlpanel.Controls.Add(this.NametextBox);
            this.Controlpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Controlpanel.Location = new System.Drawing.Point(0, 0);
            this.Controlpanel.Name = "Controlpanel";
            this.Controlpanel.Size = new System.Drawing.Size(1024, 156);
            this.Controlpanel.TabIndex = 0;
            // 
            // ImageAddresstextBox
            // 
            this.ImageAddresstextBox.Location = new System.Drawing.Point(554, 76);
            this.ImageAddresstextBox.Name = "ImageAddresstextBox";
            this.ImageAddresstextBox.Size = new System.Drawing.Size(224, 20);
            this.ImageAddresstextBox.TabIndex = 5;
            // 
            // PicAddbutton
            // 
            this.PicAddbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PicAddbutton.Image = global::SMSS.Properties.Resources.Add_user_icon_24;
            this.PicAddbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.PicAddbutton.Location = new System.Drawing.Point(823, 115);
            this.PicAddbutton.Name = "PicAddbutton";
            this.PicAddbutton.Size = new System.Drawing.Size(111, 31);
            this.PicAddbutton.TabIndex = 4;
            this.PicAddbutton.Text = "&Pic Browse";
            this.PicAddbutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.PicAddbutton.UseVisualStyleBackColor = true;
            this.PicAddbutton.Click += new System.EventHandler(this.PicAddbutton_Click);
            // 
            // StaffpictureBox
            // 
            this.StaffpictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StaffpictureBox.Image = global::SMSS.Properties.Resources.Add_user_icon_24;
            this.StaffpictureBox.Location = new System.Drawing.Point(823, 10);
            this.StaffpictureBox.Name = "StaffpictureBox";
            this.StaffpictureBox.Size = new System.Drawing.Size(111, 99);
            this.StaffpictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.StaffpictureBox.TabIndex = 3;
            this.StaffpictureBox.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "User Name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(551, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 15);
            this.label7.TabIndex = 2;
            this.label7.Text = "Status:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(291, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 15);
            this.label6.TabIndex = 2;
            this.label6.Text = "Role:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(291, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Phone_2:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(291, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Phone_1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Full Name:";
            // 
            // StatuscomboBox
            // 
            this.StatuscomboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.StatuscomboBox.FormattingEnabled = true;
            this.StatuscomboBox.Items.AddRange(new object[] {
            "Active",
            "In-Active"});
            this.StatuscomboBox.Location = new System.Drawing.Point(554, 28);
            this.StatuscomboBox.Name = "StatuscomboBox";
            this.StatuscomboBox.Size = new System.Drawing.Size(224, 21);
            this.StatuscomboBox.TabIndex = 1;
            // 
            // RolecomboBox
            // 
            this.RolecomboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RolecomboBox.FormattingEnabled = true;
            this.RolecomboBox.Location = new System.Drawing.Point(293, 118);
            this.RolecomboBox.Name = "RolecomboBox";
            this.RolecomboBox.Size = new System.Drawing.Size(224, 21);
            this.RolecomboBox.TabIndex = 1;
            // 
            // Phone2textBox
            // 
            this.Phone2textBox.Location = new System.Drawing.Point(293, 75);
            this.Phone2textBox.Name = "Phone2textBox";
            this.Phone2textBox.Size = new System.Drawing.Size(224, 20);
            this.Phone2textBox.TabIndex = 0;
            // 
            // Phone1textBox
            // 
            this.Phone1textBox.Location = new System.Drawing.Point(293, 29);
            this.Phone1textBox.Name = "Phone1textBox";
            this.Phone1textBox.Size = new System.Drawing.Size(224, 20);
            this.Phone1textBox.TabIndex = 0;
            // 
            // PasswordtextBox
            // 
            this.PasswordtextBox.Location = new System.Drawing.Point(31, 118);
            this.PasswordtextBox.Name = "PasswordtextBox";
            this.PasswordtextBox.Size = new System.Drawing.Size(224, 20);
            this.PasswordtextBox.TabIndex = 0;
            // 
            // UserNametextBox
            // 
            this.UserNametextBox.Location = new System.Drawing.Point(31, 75);
            this.UserNametextBox.Name = "UserNametextBox";
            this.UserNametextBox.Size = new System.Drawing.Size(224, 20);
            this.UserNametextBox.TabIndex = 0;
            // 
            // NametextBox
            // 
            this.NametextBox.Location = new System.Drawing.Point(31, 30);
            this.NametextBox.Name = "NametextBox";
            this.NametextBox.Size = new System.Drawing.Size(224, 20);
            this.NametextBox.TabIndex = 0;
            // 
            // Operationpanel
            // 
            this.Operationpanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Operationpanel.Controls.Add(this.SearchgroupBox);
            this.Operationpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Operationpanel.Location = new System.Drawing.Point(0, 156);
            this.Operationpanel.Name = "Operationpanel";
            this.Operationpanel.Size = new System.Drawing.Size(1024, 73);
            this.Operationpanel.TabIndex = 1;
            // 
            // SearchgroupBox
            // 
            this.SearchgroupBox.Controls.Add(this.tableLayoutPanel1);
            this.SearchgroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchgroupBox.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchgroupBox.Location = new System.Drawing.Point(0, 0);
            this.SearchgroupBox.Name = "SearchgroupBox";
            this.SearchgroupBox.Size = new System.Drawing.Size(1022, 71);
            this.SearchgroupBox.TabIndex = 0;
            this.SearchgroupBox.TabStop = false;
            this.SearchgroupBox.Text = "&Searching Option";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.SByNmradioButton, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.SByUNradioButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.SByPhradioButton, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.SearchTextgroupBox, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1016, 50);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // SByNmradioButton
            // 
            this.SByNmradioButton.AutoSize = true;
            this.SByNmradioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SByNmradioButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SByNmradioButton.Location = new System.Drawing.Point(3, 3);
            this.SByNmradioButton.Name = "SByNmradioButton";
            this.SByNmradioButton.Size = new System.Drawing.Size(248, 44);
            this.SByNmradioButton.TabIndex = 0;
            this.SByNmradioButton.TabStop = true;
            this.SByNmradioButton.Text = "&Search By Name";
            this.SByNmradioButton.UseVisualStyleBackColor = true;
            // 
            // SByUNradioButton
            // 
            this.SByUNradioButton.AutoSize = true;
            this.SByUNradioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SByUNradioButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SByUNradioButton.Location = new System.Drawing.Point(257, 3);
            this.SByUNradioButton.Name = "SByUNradioButton";
            this.SByUNradioButton.Size = new System.Drawing.Size(248, 44);
            this.SByUNradioButton.TabIndex = 0;
            this.SByUNradioButton.TabStop = true;
            this.SByUNradioButton.Text = "&Search By UserName";
            this.SByUNradioButton.UseVisualStyleBackColor = true;
            // 
            // SByPhradioButton
            // 
            this.SByPhradioButton.AutoSize = true;
            this.SByPhradioButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SByPhradioButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SByPhradioButton.Location = new System.Drawing.Point(511, 3);
            this.SByPhradioButton.Name = "SByPhradioButton";
            this.SByPhradioButton.Size = new System.Drawing.Size(248, 44);
            this.SByPhradioButton.TabIndex = 0;
            this.SByPhradioButton.TabStop = true;
            this.SByPhradioButton.Text = "&Search By Phone_No";
            this.SByPhradioButton.UseVisualStyleBackColor = true;
            // 
            // SearchTextgroupBox
            // 
            this.SearchTextgroupBox.Controls.Add(this.SearchtextBox);
            this.SearchTextgroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchTextgroupBox.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchTextgroupBox.Location = new System.Drawing.Point(765, 3);
            this.SearchTextgroupBox.Name = "SearchTextgroupBox";
            this.SearchTextgroupBox.Size = new System.Drawing.Size(248, 44);
            this.SearchTextgroupBox.TabIndex = 1;
            this.SearchTextgroupBox.TabStop = false;
            this.SearchTextgroupBox.Text = "Search";
            // 
            // SearchtextBox
            // 
            this.SearchtextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchtextBox.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchtextBox.Location = new System.Drawing.Point(3, 18);
            this.SearchtextBox.Name = "SearchtextBox";
            this.SearchtextBox.Size = new System.Drawing.Size(242, 24);
            this.SearchtextBox.TabIndex = 0;
            // 
            // GridViewpanel
            // 
            this.GridViewpanel.Controls.Add(this.StaffdataGridView);
            this.GridViewpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridViewpanel.Location = new System.Drawing.Point(0, 229);
            this.GridViewpanel.Name = "GridViewpanel";
            this.GridViewpanel.Size = new System.Drawing.Size(1024, 253);
            this.GridViewpanel.TabIndex = 2;
            // 
            // StaffdataGridView
            // 
            this.StaffdataGridView.AllowUserToAddRows = false;
            this.StaffdataGridView.AllowUserToDeleteRows = false;
            this.StaffdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.StaffdataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.StaffdataGridView.BackgroundColor = System.Drawing.Color.White;
            this.StaffdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StaffdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SGvNo,
            this.SGvID,
            this.SGvName,
            this.SGvUN,
            this.SGvPW,
            this.SGvPh_1,
            this.SGvPh_2,
            this.SGvRole,
            this.SGvSt});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.StaffdataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.StaffdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StaffdataGridView.Location = new System.Drawing.Point(0, 0);
            this.StaffdataGridView.Name = "StaffdataGridView";
            this.StaffdataGridView.ReadOnly = true;
            this.StaffdataGridView.RowHeadersVisible = false;
            this.StaffdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.StaffdataGridView.Size = new System.Drawing.Size(1024, 253);
            this.StaffdataGridView.TabIndex = 0;
            this.StaffdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.StaffdataGridView_CellClick);
            // 
            // SGvNo
            // 
            this.SGvNo.HeaderText = "Sno";
            this.SGvNo.Name = "SGvNo";
            this.SGvNo.ReadOnly = true;
            // 
            // SGvID
            // 
            this.SGvID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.SGvID.HeaderText = "ID";
            this.SGvID.Name = "SGvID";
            this.SGvID.ReadOnly = true;
            this.SGvID.Visible = false;
            // 
            // SGvName
            // 
            this.SGvName.HeaderText = "Name";
            this.SGvName.Name = "SGvName";
            this.SGvName.ReadOnly = true;
            // 
            // SGvUN
            // 
            this.SGvUN.HeaderText = "User Name";
            this.SGvUN.Name = "SGvUN";
            this.SGvUN.ReadOnly = true;
            // 
            // SGvPW
            // 
            this.SGvPW.HeaderText = "Password";
            this.SGvPW.Name = "SGvPW";
            this.SGvPW.ReadOnly = true;
            // 
            // SGvPh_1
            // 
            this.SGvPh_1.HeaderText = "Phone 1";
            this.SGvPh_1.Name = "SGvPh_1";
            this.SGvPh_1.ReadOnly = true;
            // 
            // SGvPh_2
            // 
            this.SGvPh_2.HeaderText = "Phone 2";
            this.SGvPh_2.Name = "SGvPh_2";
            this.SGvPh_2.ReadOnly = true;
            // 
            // SGvRole
            // 
            this.SGvRole.HeaderText = "Role";
            this.SGvRole.Name = "SGvRole";
            this.SGvRole.ReadOnly = true;
            // 
            // SGvSt
            // 
            this.SGvSt.HeaderText = "Status";
            this.SGvSt.Name = "SGvSt";
            this.SGvSt.ReadOnly = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(166, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(385, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(245, 30);
            this.label8.TabIndex = 1;
            this.label8.Text = "Staff Registeration Form";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(197, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(161, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "Alied Group of Schools";
            // 
            // StaffPicopenFileDialog
            // 
            this.StaffPicopenFileDialog.FileName = "openFileDialog1";
            // 
            // StaffForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 611);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "StaffForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StaffForm";
            this.Load += new System.EventHandler(this.StaffForm_Load);
            this.Toppanel.ResumeLayout(false);
            this.Toppanel.PerformLayout();
            this.Centerpanel.ResumeLayout(false);
            this.Controlpanel.ResumeLayout(false);
            this.Controlpanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StaffpictureBox)).EndInit();
            this.Operationpanel.ResumeLayout(false);
            this.SearchgroupBox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.SearchTextgroupBox.ResumeLayout(false);
            this.SearchTextgroupBox.PerformLayout();
            this.GridViewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StaffdataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Operationpanel;
        private System.Windows.Forms.Panel Controlpanel;
        private System.Windows.Forms.Button PicAddbutton;
        private System.Windows.Forms.PictureBox StaffpictureBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox StatuscomboBox;
        private System.Windows.Forms.ComboBox RolecomboBox;
        private System.Windows.Forms.TextBox Phone2textBox;
        private System.Windows.Forms.TextBox Phone1textBox;
        private System.Windows.Forms.TextBox PasswordtextBox;
        private System.Windows.Forms.TextBox UserNametextBox;
        private System.Windows.Forms.TextBox NametextBox;
        private System.Windows.Forms.Panel GridViewpanel;
        private System.Windows.Forms.DataGridView StaffdataGridView;
        private System.Windows.Forms.GroupBox SearchgroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RadioButton SByNmradioButton;
        private System.Windows.Forms.RadioButton SByUNradioButton;
        private System.Windows.Forms.RadioButton SByPhradioButton;
        private System.Windows.Forms.GroupBox SearchTextgroupBox;
        private System.Windows.Forms.TextBox SearchtextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvPW;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvPh_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvPh_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvRole;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGvSt;
        private System.Windows.Forms.OpenFileDialog StaffPicopenFileDialog;
        private System.Windows.Forms.TextBox ImageAddresstextBox;
    }
}