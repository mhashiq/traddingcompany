﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS.Screens
{
    public partial class Dashboard : mainLayout
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ClosepictureBox_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MinimizepictureBox_Click(object sender, EventArgs e)
        {
            if(this.WindowState!=FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void Rolebutton_Click(object sender, EventArgs e)
        {
            Roles r = new Roles();
            r.MdiParent = MDIMain.ActiveForm;
           // this.Close();
            r.Show();
        }

        private void Admissionbutton_Click(object sender, EventArgs e)
        {
            //AdmissionForm af = new AdmissionForm();
            //af.MdiParent = MDIMain.ActiveForm;
            //af.Show();
        }

        private void Staffbutton_Click(object sender, EventArgs e)
        {
            StaffForm sf = new StaffForm();
            sf.MdiParent = MDIMain.ActiveForm;
            sf.Show();
        }

        private void Sectionbutton_Click(object sender, EventArgs e)
        {
            Section s = new Section();
                s.MdiParent = MDIMain.ActiveForm;
            s.Show();
        }

        private void Clsbutton_Click(object sender, EventArgs e)
        {
            Standerd s = new Standerd();
            s.MdiParent = MDIMain.ActiveForm;
            s.Show();
        }

        private void Subjectbutton_Click(object sender, EventArgs e)
        {
            Subjects s = new Subjects();
            s.MdiParent = MDIMain.ActiveForm;
            s.Show();
        }

        private void Shiftbutton_Click(object sender, EventArgs e)
        {
            Shifts s = new Shifts();
            s.MdiParent = MDIMain.ActiveForm;
            s.Show();
        }

        private void ClassSchedule_Click(object sender, EventArgs e)
        {
            CSchedule c = new CSchedule();
            c.MdiParent = MDIMain.ActiveForm;
            c.Show();
            //DumyScheduleLoad d = new DumyScheduleLoad();
            //d.MdiParent = MDIMain.ActiveForm;
            //d.Show();
        }
    }
}
