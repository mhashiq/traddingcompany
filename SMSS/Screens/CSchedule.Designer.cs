﻿namespace SMSS.Screens
{
    partial class CSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TimingCtrlPnel = new System.Windows.Forms.Panel();
            this.TimingCtrlGrp = new System.Windows.Forms.GroupBox();
            this.DayscomboBox = new System.Windows.Forms.ComboBox();
            this.TodateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.FromdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.ShiftcomboBox = new System.Windows.Forms.ComboBox();
            this.NametextBox = new System.Windows.Forms.TextBox();
            this.GridViewpanel = new System.Windows.Forms.Panel();
            this.TimingdataGridView = new System.Windows.Forms.DataGridView();
            this.ClassescomboBox = new System.Windows.Forms.ComboBox();
            this.TSno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TGvId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TcGvId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TsGvId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TGvName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TcGvName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TsGvName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TGvDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TGvFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TGvTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Centerpanel.SuspendLayout();
            this.TimingCtrlPnel.SuspendLayout();
            this.TimingCtrlGrp.SuspendLayout();
            this.GridViewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimingdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Toppanel
            // 
            this.Toppanel.Size = new System.Drawing.Size(684, 76);
            // 
            // ButtonControlpanel
            // 
            this.ButtonControlpanel.Size = new System.Drawing.Size(684, 53);
            // 
            // Centerpanel
            // 
            this.Centerpanel.Controls.Add(this.GridViewpanel);
            this.Centerpanel.Controls.Add(this.TimingCtrlPnel);
            this.Centerpanel.Size = new System.Drawing.Size(684, 282);
            // 
            // TimingCtrlPnel
            // 
            this.TimingCtrlPnel.Controls.Add(this.TimingCtrlGrp);
            this.TimingCtrlPnel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TimingCtrlPnel.Location = new System.Drawing.Point(0, 0);
            this.TimingCtrlPnel.Name = "TimingCtrlPnel";
            this.TimingCtrlPnel.Size = new System.Drawing.Size(684, 142);
            this.TimingCtrlPnel.TabIndex = 0;
            // 
            // TimingCtrlGrp
            // 
            this.TimingCtrlGrp.Controls.Add(this.ClassescomboBox);
            this.TimingCtrlGrp.Controls.Add(this.DayscomboBox);
            this.TimingCtrlGrp.Controls.Add(this.TodateTimePicker);
            this.TimingCtrlGrp.Controls.Add(this.FromdateTimePicker);
            this.TimingCtrlGrp.Controls.Add(this.ShiftcomboBox);
            this.TimingCtrlGrp.Controls.Add(this.NametextBox);
            this.TimingCtrlGrp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimingCtrlGrp.Location = new System.Drawing.Point(0, 0);
            this.TimingCtrlGrp.Name = "TimingCtrlGrp";
            this.TimingCtrlGrp.Size = new System.Drawing.Size(684, 142);
            this.TimingCtrlGrp.TabIndex = 0;
            this.TimingCtrlGrp.TabStop = false;
            this.TimingCtrlGrp.Text = "&Schedule &Set:";
            // 
            // DayscomboBox
            // 
            this.DayscomboBox.FormattingEnabled = true;
            this.DayscomboBox.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.DayscomboBox.Location = new System.Drawing.Point(393, 113);
            this.DayscomboBox.Name = "DayscomboBox";
            this.DayscomboBox.Size = new System.Drawing.Size(190, 21);
            this.DayscomboBox.TabIndex = 4;
            // 
            // TodateTimePicker
            // 
            this.TodateTimePicker.CustomFormat = "hh:mm tt";
            this.TodateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.TodateTimePicker.Location = new System.Drawing.Point(393, 70);
            this.TodateTimePicker.Name = "TodateTimePicker";
            this.TodateTimePicker.ShowUpDown = true;
            this.TodateTimePicker.Size = new System.Drawing.Size(190, 20);
            this.TodateTimePicker.TabIndex = 3;
            // 
            // FromdateTimePicker
            // 
            this.FromdateTimePicker.CustomFormat = "hh:mm tt";
            this.FromdateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.FromdateTimePicker.Location = new System.Drawing.Point(393, 26);
            this.FromdateTimePicker.Name = "FromdateTimePicker";
            this.FromdateTimePicker.ShowUpDown = true;
            this.FromdateTimePicker.Size = new System.Drawing.Size(190, 20);
            this.FromdateTimePicker.TabIndex = 2;
            // 
            // ShiftcomboBox
            // 
            this.ShiftcomboBox.FormattingEnabled = true;
            this.ShiftcomboBox.Location = new System.Drawing.Point(78, 72);
            this.ShiftcomboBox.Name = "ShiftcomboBox";
            this.ShiftcomboBox.Size = new System.Drawing.Size(190, 21);
            this.ShiftcomboBox.TabIndex = 1;
            // 
            // NametextBox
            // 
            this.NametextBox.Location = new System.Drawing.Point(78, 29);
            this.NametextBox.Name = "NametextBox";
            this.NametextBox.Size = new System.Drawing.Size(190, 20);
            this.NametextBox.TabIndex = 0;
            // 
            // GridViewpanel
            // 
            this.GridViewpanel.Controls.Add(this.TimingdataGridView);
            this.GridViewpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridViewpanel.Location = new System.Drawing.Point(0, 142);
            this.GridViewpanel.Name = "GridViewpanel";
            this.GridViewpanel.Size = new System.Drawing.Size(684, 140);
            this.GridViewpanel.TabIndex = 1;
            // 
            // TimingdataGridView
            // 
            this.TimingdataGridView.AllowUserToAddRows = false;
            this.TimingdataGridView.AllowUserToDeleteRows = false;
            this.TimingdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TimingdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TimingdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TSno,
            this.TGvId,
            this.TcGvId,
            this.TsGvId,
            this.TGvName,
            this.TcGvName,
            this.TsGvName,
            this.TGvDays,
            this.TGvFrom,
            this.TGvTo});
            this.TimingdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimingdataGridView.Location = new System.Drawing.Point(0, 0);
            this.TimingdataGridView.Name = "TimingdataGridView";
            this.TimingdataGridView.ReadOnly = true;
            this.TimingdataGridView.RowHeadersVisible = false;
            this.TimingdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TimingdataGridView.Size = new System.Drawing.Size(684, 140);
            this.TimingdataGridView.TabIndex = 0;
            this.TimingdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TimingdataGridView_CellClick);
            // 
            // ClassescomboBox
            // 
            this.ClassescomboBox.FormattingEnabled = true;
            this.ClassescomboBox.Location = new System.Drawing.Point(78, 113);
            this.ClassescomboBox.Name = "ClassescomboBox";
            this.ClassescomboBox.Size = new System.Drawing.Size(190, 21);
            this.ClassescomboBox.TabIndex = 5;
            // 
            // TSno
            // 
            this.TSno.HeaderText = "Sno";
            this.TSno.Name = "TSno";
            this.TSno.ReadOnly = true;
            // 
            // TGvId
            // 
            this.TGvId.HeaderText = "Id";
            this.TGvId.Name = "TGvId";
            this.TGvId.ReadOnly = true;
            this.TGvId.Visible = false;
            // 
            // TcGvId
            // 
            this.TcGvId.HeaderText = "CID";
            this.TcGvId.Name = "TcGvId";
            this.TcGvId.ReadOnly = true;
            this.TcGvId.Visible = false;
            // 
            // TsGvId
            // 
            this.TsGvId.HeaderText = "SId";
            this.TsGvId.Name = "TsGvId";
            this.TsGvId.ReadOnly = true;
            this.TsGvId.Visible = false;
            // 
            // TGvName
            // 
            this.TGvName.HeaderText = "Name";
            this.TGvName.Name = "TGvName";
            this.TGvName.ReadOnly = true;
            // 
            // TcGvName
            // 
            this.TcGvName.HeaderText = "Class Name";
            this.TcGvName.Name = "TcGvName";
            this.TcGvName.ReadOnly = true;
            // 
            // TsGvName
            // 
            this.TsGvName.HeaderText = "Shift Name";
            this.TsGvName.Name = "TsGvName";
            this.TsGvName.ReadOnly = true;
            // 
            // TGvDays
            // 
            this.TGvDays.HeaderText = "Days";
            this.TGvDays.Name = "TGvDays";
            this.TGvDays.ReadOnly = true;
            // 
            // TGvFrom
            // 
            this.TGvFrom.HeaderText = "Timing From";
            this.TGvFrom.Name = "TGvFrom";
            this.TGvFrom.ReadOnly = true;
            // 
            // TGvTo
            // 
            this.TGvTo.HeaderText = "Timig To";
            this.TGvTo.Name = "TGvTo";
            this.TGvTo.ReadOnly = true;
            // 
            // CSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.MaximizeBox = false;
            this.Name = "CSchedule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Class Schedule";
            this.Load += new System.EventHandler(this.CSchedule_Load);
            this.Centerpanel.ResumeLayout(false);
            this.TimingCtrlPnel.ResumeLayout(false);
            this.TimingCtrlGrp.ResumeLayout(false);
            this.TimingCtrlGrp.PerformLayout();
            this.GridViewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TimingdataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel TimingCtrlPnel;
        private System.Windows.Forms.GroupBox TimingCtrlGrp;
        private System.Windows.Forms.ComboBox DayscomboBox;
        private System.Windows.Forms.DateTimePicker TodateTimePicker;
        private System.Windows.Forms.DateTimePicker FromdateTimePicker;
        private System.Windows.Forms.ComboBox ShiftcomboBox;
        private System.Windows.Forms.TextBox NametextBox;
        private System.Windows.Forms.Panel GridViewpanel;
        private System.Windows.Forms.DataGridView TimingdataGridView;
        private System.Windows.Forms.ComboBox ClassescomboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn TSno;
        private System.Windows.Forms.DataGridViewTextBoxColumn TGvId;
        private System.Windows.Forms.DataGridViewTextBoxColumn TcGvId;
        private System.Windows.Forms.DataGridViewTextBoxColumn TsGvId;
        private System.Windows.Forms.DataGridViewTextBoxColumn TGvName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TcGvName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TsGvName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TGvDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn TGvFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn TGvTo;
    }
}