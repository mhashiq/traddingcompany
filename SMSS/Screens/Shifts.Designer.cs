﻿namespace SMSS.Screens
{
    partial class Shifts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Shiftpanel = new System.Windows.Forms.Panel();
            this.ShiftgroupBox = new System.Windows.Forms.GroupBox();
            this.ToDateTime = new System.Windows.Forms.DateTimePicker();
            this.FromDateTime = new System.Windows.Forms.DateTimePicker();
            this.ShiftNumtextBox = new System.Windows.Forms.TextBox();
            this.ShiftNametextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ShiftGridViewpanel = new System.Windows.Forms.Panel();
            this.ShiftdataGridView = new System.Windows.Forms.DataGridView();
            this.ShGvSno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShGvId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShGvName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShGvNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShGvFromT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShGvToT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Centerpanel.SuspendLayout();
            this.Shiftpanel.SuspendLayout();
            this.ShiftgroupBox.SuspendLayout();
            this.ShiftGridViewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Toppanel
            // 
            this.Toppanel.Size = new System.Drawing.Size(684, 76);
            // 
            // ButtonControlpanel
            // 
            this.ButtonControlpanel.Size = new System.Drawing.Size(684, 53);
            // 
            // Centerpanel
            // 
            this.Centerpanel.Controls.Add(this.ShiftGridViewpanel);
            this.Centerpanel.Controls.Add(this.Shiftpanel);
            this.Centerpanel.Size = new System.Drawing.Size(684, 282);
            // 
            // Shiftpanel
            // 
            this.Shiftpanel.Controls.Add(this.ShiftgroupBox);
            this.Shiftpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Shiftpanel.Location = new System.Drawing.Point(0, 0);
            this.Shiftpanel.Name = "Shiftpanel";
            this.Shiftpanel.Size = new System.Drawing.Size(684, 140);
            this.Shiftpanel.TabIndex = 0;
            // 
            // ShiftgroupBox
            // 
            this.ShiftgroupBox.Controls.Add(this.label2);
            this.ShiftgroupBox.Controls.Add(this.label4);
            this.ShiftgroupBox.Controls.Add(this.label3);
            this.ShiftgroupBox.Controls.Add(this.label1);
            this.ShiftgroupBox.Controls.Add(this.ToDateTime);
            this.ShiftgroupBox.Controls.Add(this.FromDateTime);
            this.ShiftgroupBox.Controls.Add(this.ShiftNumtextBox);
            this.ShiftgroupBox.Controls.Add(this.ShiftNametextBox);
            this.ShiftgroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShiftgroupBox.Location = new System.Drawing.Point(0, 0);
            this.ShiftgroupBox.Name = "ShiftgroupBox";
            this.ShiftgroupBox.Size = new System.Drawing.Size(684, 140);
            this.ShiftgroupBox.TabIndex = 0;
            this.ShiftgroupBox.TabStop = false;
            this.ShiftgroupBox.Text = "Shifts Entry";
            // 
            // ToDateTime
            // 
            this.ToDateTime.CustomFormat = "hh:mm tt";
            this.ToDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.ToDateTime.Location = new System.Drawing.Point(401, 70);
            this.ToDateTime.Name = "ToDateTime";
            this.ToDateTime.ShowUpDown = true;
            this.ToDateTime.Size = new System.Drawing.Size(200, 20);
            this.ToDateTime.TabIndex = 1;
            // 
            // FromDateTime
            // 
            this.FromDateTime.CustomFormat = "hh:mm tt";
            this.FromDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.FromDateTime.Location = new System.Drawing.Point(401, 30);
            this.FromDateTime.Name = "FromDateTime";
            this.FromDateTime.ShowUpDown = true;
            this.FromDateTime.Size = new System.Drawing.Size(200, 20);
            this.FromDateTime.TabIndex = 1;
            // 
            // ShiftNumtextBox
            // 
            this.ShiftNumtextBox.Location = new System.Drawing.Point(116, 73);
            this.ShiftNumtextBox.Name = "ShiftNumtextBox";
            this.ShiftNumtextBox.Size = new System.Drawing.Size(200, 20);
            this.ShiftNumtextBox.TabIndex = 0;
            // 
            // ShiftNametextBox
            // 
            this.ShiftNametextBox.Location = new System.Drawing.Point(116, 33);
            this.ShiftNametextBox.Name = "ShiftNametextBox";
            this.ShiftNametextBox.Size = new System.Drawing.Size(200, 20);
            this.ShiftNametextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Shift Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Shift Number:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(361, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "From:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(371, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "To:";
            // 
            // ShiftGridViewpanel
            // 
            this.ShiftGridViewpanel.Controls.Add(this.ShiftdataGridView);
            this.ShiftGridViewpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShiftGridViewpanel.Location = new System.Drawing.Point(0, 140);
            this.ShiftGridViewpanel.Name = "ShiftGridViewpanel";
            this.ShiftGridViewpanel.Size = new System.Drawing.Size(684, 142);
            this.ShiftGridViewpanel.TabIndex = 1;
            // 
            // ShiftdataGridView
            // 
            this.ShiftdataGridView.AllowUserToAddRows = false;
            this.ShiftdataGridView.AllowUserToDeleteRows = false;
            this.ShiftdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ShiftdataGridView.BackgroundColor = System.Drawing.Color.White;
            this.ShiftdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ShiftdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ShGvSno,
            this.ShGvId,
            this.ShGvName,
            this.ShGvNum,
            this.ShGvFromT,
            this.ShGvToT});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ShiftdataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.ShiftdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShiftdataGridView.Location = new System.Drawing.Point(0, 0);
            this.ShiftdataGridView.Name = "ShiftdataGridView";
            this.ShiftdataGridView.ReadOnly = true;
            this.ShiftdataGridView.RowHeadersVisible = false;
            this.ShiftdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ShiftdataGridView.Size = new System.Drawing.Size(684, 142);
            this.ShiftdataGridView.TabIndex = 0;
            // 
            // ShGvSno
            // 
            this.ShGvSno.HeaderText = "Sno";
            this.ShGvSno.Name = "ShGvSno";
            this.ShGvSno.ReadOnly = true;
            // 
            // ShGvId
            // 
            this.ShGvId.HeaderText = "ID";
            this.ShGvId.Name = "ShGvId";
            this.ShGvId.ReadOnly = true;
            this.ShGvId.Visible = false;
            // 
            // ShGvName
            // 
            this.ShGvName.HeaderText = "Shift Name";
            this.ShGvName.Name = "ShGvName";
            this.ShGvName.ReadOnly = true;
            // 
            // ShGvNum
            // 
            this.ShGvNum.HeaderText = "Shift Number";
            this.ShGvNum.Name = "ShGvNum";
            this.ShGvNum.ReadOnly = true;
            // 
            // ShGvFromT
            // 
            this.ShGvFromT.HeaderText = "From Time";
            this.ShGvFromT.Name = "ShGvFromT";
            this.ShGvFromT.ReadOnly = true;
            // 
            // ShGvToT
            // 
            this.ShGvToT.HeaderText = "To Time";
            this.ShGvToT.Name = "ShGvToT";
            this.ShGvToT.ReadOnly = true;
            // 
            // Shifts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.Name = "Shifts";
            this.Text = "Shifts";
            this.Load += new System.EventHandler(this.Shifts_Load);
            this.Centerpanel.ResumeLayout(false);
            this.Shiftpanel.ResumeLayout(false);
            this.ShiftgroupBox.ResumeLayout(false);
            this.ShiftgroupBox.PerformLayout();
            this.ShiftGridViewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ShiftdataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Shiftpanel;
        private System.Windows.Forms.GroupBox ShiftgroupBox;
        private System.Windows.Forms.DateTimePicker ToDateTime;
        private System.Windows.Forms.DateTimePicker FromDateTime;
        private System.Windows.Forms.TextBox ShiftNumtextBox;
        private System.Windows.Forms.TextBox ShiftNametextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel ShiftGridViewpanel;
        private System.Windows.Forms.DataGridView ShiftdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShGvSno;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShGvId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShGvName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShGvNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShGvFromT;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShGvToT;
    }
}