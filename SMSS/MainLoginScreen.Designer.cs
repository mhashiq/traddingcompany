﻿namespace SMSS
{
    partial class MainLoginScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.MinimizepictureBox = new System.Windows.Forms.PictureBox();
            this.ClosepictureBox = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.PasswordtextBox = new System.Windows.Forms.TextBox();
            this.UsernametextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Loginbutton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizepictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosepictureBox)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            this.panel1.Controls.Add(this.MinimizepictureBox);
            this.panel1.Controls.Add(this.ClosepictureBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(676, 61);
            this.panel1.TabIndex = 0;
            // 
            // MinimizepictureBox
            // 
            this.MinimizepictureBox.Image = global::SMSS.Properties.Resources.Knob_Remove_icon_minimize_32;
            this.MinimizepictureBox.Location = new System.Drawing.Point(590, 12);
            this.MinimizepictureBox.Name = "MinimizepictureBox";
            this.MinimizepictureBox.Size = new System.Drawing.Size(34, 31);
            this.MinimizepictureBox.TabIndex = 1;
            this.MinimizepictureBox.TabStop = false;
            this.MinimizepictureBox.Click += new System.EventHandler(this.MinimizepictureBox_Click);
            // 
            // ClosepictureBox
            // 
            this.ClosepictureBox.Image = global::SMSS.Properties.Resources.Knob_Cancel_icon_32;
            this.ClosepictureBox.Location = new System.Drawing.Point(630, 12);
            this.ClosepictureBox.Name = "ClosepictureBox";
            this.ClosepictureBox.Size = new System.Drawing.Size(34, 31);
            this.ClosepictureBox.TabIndex = 0;
            this.ClosepictureBox.TabStop = false;
            this.ClosepictureBox.Click += new System.EventHandler(this.ClosepictureBox_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 61);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(287, 269);
            this.panel2.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::SMSS.Properties.Resources.Actions_system_lock_screen_icon_128;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(287, 269);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.PasswordtextBox);
            this.panel3.Controls.Add(this.UsernametextBox);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.Loginbutton);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(287, 61);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(389, 269);
            this.panel3.TabIndex = 2;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(99, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Password:";
            // 
            // PasswordtextBox
            // 
            this.PasswordtextBox.Location = new System.Drawing.Point(102, 132);
            this.PasswordtextBox.MaxLength = 30;
            this.PasswordtextBox.Name = "PasswordtextBox";
            this.PasswordtextBox.PasswordChar = '*';
            this.PasswordtextBox.Size = new System.Drawing.Size(188, 20);
            this.PasswordtextBox.TabIndex = 7;
            // 
            // UsernametextBox
            // 
            this.UsernametextBox.Location = new System.Drawing.Point(102, 89);
            this.UsernametextBox.MaxLength = 50;
            this.UsernametextBox.Name = "UsernametextBox";
            this.UsernametextBox.Size = new System.Drawing.Size(188, 20);
            this.UsernametextBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(102, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "User Name:";
            // 
            // Loginbutton
            // 
            this.Loginbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Loginbutton.Image = global::SMSS.Properties.Resources.Key_icon_24;
            this.Loginbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Loginbutton.Location = new System.Drawing.Point(102, 165);
            this.Loginbutton.Name = "Loginbutton";
            this.Loginbutton.Size = new System.Drawing.Size(188, 32);
            this.Loginbutton.TabIndex = 6;
            this.Loginbutton.TabStop = false;
            this.Loginbutton.Text = "Login";
            this.Loginbutton.UseVisualStyleBackColor = true;
            this.Loginbutton.Click += new System.EventHandler(this.Loginbutton_Click);
            // 
            // MainLoginScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 330);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainLoginScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainLoginScreen";
            this.Load += new System.EventHandler(this.MainLoginScreen_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MinimizepictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosepictureBox)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PasswordtextBox;
        private System.Windows.Forms.TextBox UsernametextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Loginbutton;
        private System.Windows.Forms.PictureBox ClosepictureBox;
        private System.Windows.Forms.PictureBox MinimizepictureBox;
    }
}