﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS
{
   public class UtitlityClass
    {
        public static string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public static int count;
        public static DialogResult ShowMsg(string msg,string heading,string type)
        {

            if (type == "Success")
            {
                return MessageBox.Show(msg, heading, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                return MessageBox.Show(msg, heading, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static void Disable_Reset(Panel p)
        {
            foreach(Control c in p.Controls)
            {
                if(c is TextBox)
                {
                    TextBox t = (TextBox)c;
                    t.Enabled = false;
                    t.Text = "";
                }
                if(c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
                if (c is PictureBox)
                {
                    PictureBox pb = (PictureBox)c;
                    pb.Image = null;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = false;
                    cb.SelectedIndex = -1;
                }
                if(c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    rb.Enabled = false;
                    rb.Checked = false;
                }
                if(c is CheckBox)
                {
                    CheckBox cbb = (CheckBox)c;
                    cbb.Enabled = false;
                    cbb.Checked = false;
                }
                if(c is DateTimePicker)
                {
                    DateTimePicker dp = (DateTimePicker)c;
                    dp.Enabled = false;
                    dp.Value = DateTime.Now;
                }
            }
        }
        public static void Disable_Reset(GroupBox gb)
        {
            foreach (Control c in gb.Controls)
            {
                if (c is TextBox)
                {
                    TextBox t = (TextBox)c;
                    t.Enabled = false;
                    t.Text = "";
                }
                if (c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
                if (c is PictureBox)
                {
                    PictureBox pb = (PictureBox)c;
                    pb.Image = null;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = false;
                    cb.SelectedIndex = -1;
                }
                if (c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    rb.Enabled = false;
                    rb.Checked = false;
                }
                if (c is CheckBox)
                {
                    CheckBox cbb = (CheckBox)c;
                    cbb.Enabled = false;
                    cbb.Checked = false;
                }
                if (c is DateTimePicker)
                {
                    DateTimePicker dp = (DateTimePicker)c;
                    dp.Enabled = false;
                    dp.Value = DateTime.Now;
                }
            }
        }
        public static void Disable(GroupBox gb)
        {
            foreach (Control c in gb.Controls)
            {
                if (c is TextBox)
                {
                    TextBox t = (TextBox)c;
                    t.Enabled = false;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = false;
                }
                if (c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    rb.Enabled = false;
                }
                if (c is CheckBox)
                {
                    CheckBox cbb = (CheckBox)c;
                    cbb.Enabled = false;
                }
                if (c is DateTimePicker)
                {
                    DateTimePicker dp = (DateTimePicker)c;
                    dp.Enabled = false;
                    dp.Value = DateTime.Now;
                }
            }
        }
        public static void Disable(Panel p)
        {
            foreach (Control c in p.Controls)
            {
                if (c is TextBox)
                {
                    TextBox t = (TextBox)c;
                    t.Enabled = false;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = false;
                }
                if (c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    rb.Enabled = false;
                }
                if (c is CheckBox)
                {
                    CheckBox cbb = (CheckBox)c;
                    cbb.Enabled = false;
                }
                if (c is DateTimePicker)
                {
                    DateTimePicker dp = (DateTimePicker)c;
                    dp.Enabled = false;
                    dp.Value = DateTime.Now;
                }
            }
        }
        public static void Enable_Reset(Panel p)
        {
            foreach (Control c in p.Controls)
            {
                if (c is TextBox)
                {
                    TextBox t = (TextBox)c;
                    t.Enabled = true;
                    t.Text = "";
                }
                if (c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                }
                if (c is PictureBox)
                {
                    PictureBox pb = (PictureBox)c;
                    pb.Image = null;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = true;
                    cb.SelectedIndex = -1;
                }
                if (c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    rb.Enabled = true;
                    rb.Checked = true;
                }
                if (c is CheckBox)
                {
                    CheckBox cbb = (CheckBox)c;
                    cbb.Enabled = true;
                    cbb.Checked = true;
                }
                if (c is DateTimePicker)
                {
                    DateTimePicker dp = (DateTimePicker)c;
                    dp.Enabled = true;
                    dp.Value = DateTime.Now;
                }
            }
        }
        public static void Enable_Reset(GroupBox gb)
        {
            foreach (Control c in gb.Controls)
            {
                if (c is TextBox)
                {
                    TextBox t = (TextBox)c;
                    t.Enabled = true;
                    t.Text = "";
                }
                if (c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                }
                if (c is PictureBox)
                {
                    PictureBox pb = (PictureBox)c;
                    pb.Image = null;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = true;
                    cb.SelectedIndex = -1;
                }
                if (c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    rb.Enabled = true;
                    rb.Checked = true;
                }
                if (c is CheckBox)
                {
                    CheckBox cbb = (CheckBox)c;
                    cbb.Enabled = true;
                    cbb.Checked = true;
                }
                if (c is DateTimePicker)
                {
                    DateTimePicker dp = (DateTimePicker)c;
                    dp.Enabled = true;
                    dp.Value = DateTime.Now;
                }
            }
        }
        public static void Enable(Panel p)
        {
            foreach (Control c in p.Controls)
            {
                if (c is TextBox)
                {
                    TextBox t = (TextBox)c;
                    t.Enabled = true;
                }
                if (c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = true;
                }
                if (c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    rb.Enabled = true;
                }
                if (c is CheckBox)
                {
                    CheckBox cbb = (CheckBox)c;
                    cbb.Enabled = true;
                }
                if (c is DateTimePicker)
                {
                    DateTimePicker dp = (DateTimePicker)c;
                    dp.Enabled = true;
                    dp.Value = DateTime.Now;
                }
            }
        }
        public static void Enable(GroupBox gb)
        {
            foreach (Control c in gb.Controls)
            {
                if (c is TextBox)
                {
                    TextBox t = (TextBox)c;
                    t.Enabled = true;
                }
                if (c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = true;
                }
                if (c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    rb.Enabled = true;
                }
                if (c is CheckBox)
                {
                    CheckBox cbb = (CheckBox)c;
                    cbb.Enabled = true;
                }
                if (c is DateTimePicker)
                {
                    DateTimePicker dp = (DateTimePicker)c;
                    dp.Enabled = true;
                    dp.Value = DateTime.Now;
                }
            }
        }
        public static void SNO(DataGridView dv,string coumn,int reset)
        {
            count = reset;
            foreach (DataGridViewRow row in dv.Rows)
            {
                row.Cells[coumn].Value = ++count;
            }
        }
    }
}
