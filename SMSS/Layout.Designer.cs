﻿namespace SMSS
{
    partial class Layout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Toppanel = new System.Windows.Forms.Panel();
            this.ButtonControlpanel = new System.Windows.Forms.Panel();
            this.ButtonControltableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.Delbutton = new System.Windows.Forms.Button();
            this.Updatebutton = new System.Windows.Forms.Button();
            this.GetRecordbutton = new System.Windows.Forms.Button();
            this.Savebutton = new System.Windows.Forms.Button();
            this.Addbutton = new System.Windows.Forms.Button();
            this.Clearbutton = new System.Windows.Forms.Button();
            this.Centerpanel = new System.Windows.Forms.Panel();
            this.ButtonControlpanel.SuspendLayout();
            this.ButtonControltableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Toppanel
            // 
            this.Toppanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(122)))));
            this.Toppanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Toppanel.Location = new System.Drawing.Point(0, 0);
            this.Toppanel.Name = "Toppanel";
            this.Toppanel.Size = new System.Drawing.Size(967, 76);
            this.Toppanel.TabIndex = 0;
            // 
            // ButtonControlpanel
            // 
            this.ButtonControlpanel.Controls.Add(this.ButtonControltableLayoutPanel);
            this.ButtonControlpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ButtonControlpanel.Location = new System.Drawing.Point(0, 76);
            this.ButtonControlpanel.Name = "ButtonControlpanel";
            this.ButtonControlpanel.Size = new System.Drawing.Size(967, 53);
            this.ButtonControlpanel.TabIndex = 1;
            // 
            // ButtonControltableLayoutPanel
            // 
            this.ButtonControltableLayoutPanel.ColumnCount = 6;
            this.ButtonControltableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.ButtonControltableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.ButtonControltableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.ButtonControltableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.ButtonControltableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.ButtonControltableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.ButtonControltableLayoutPanel.Controls.Add(this.Delbutton, 4, 0);
            this.ButtonControltableLayoutPanel.Controls.Add(this.Updatebutton, 3, 0);
            this.ButtonControltableLayoutPanel.Controls.Add(this.GetRecordbutton, 2, 0);
            this.ButtonControltableLayoutPanel.Controls.Add(this.Savebutton, 1, 0);
            this.ButtonControltableLayoutPanel.Controls.Add(this.Addbutton, 0, 0);
            this.ButtonControltableLayoutPanel.Controls.Add(this.Clearbutton, 5, 0);
            this.ButtonControltableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ButtonControltableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.ButtonControltableLayoutPanel.Name = "ButtonControltableLayoutPanel";
            this.ButtonControltableLayoutPanel.RowCount = 1;
            this.ButtonControltableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ButtonControltableLayoutPanel.Size = new System.Drawing.Size(967, 53);
            this.ButtonControltableLayoutPanel.TabIndex = 0;
            // 
            // Delbutton
            // 
            this.Delbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Delbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Delbutton.Image = global::SMSS.Properties.Resources.Knob_Cancel_icon_32;
            this.Delbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Delbutton.Location = new System.Drawing.Point(647, 3);
            this.Delbutton.Name = "Delbutton";
            this.Delbutton.Size = new System.Drawing.Size(155, 47);
            this.Delbutton.TabIndex = 4;
            this.Delbutton.Text = "&Delete";
            this.Delbutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Delbutton.UseVisualStyleBackColor = true;
            this.Delbutton.Click += new System.EventHandler(this.Delbutton_Click);
            // 
            // Updatebutton
            // 
            this.Updatebutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Updatebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Updatebutton.Image = global::SMSS.Properties.Resources.Edit_Map_icon_321;
            this.Updatebutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Updatebutton.Location = new System.Drawing.Point(486, 3);
            this.Updatebutton.Name = "Updatebutton";
            this.Updatebutton.Size = new System.Drawing.Size(155, 47);
            this.Updatebutton.TabIndex = 3;
            this.Updatebutton.Text = "&Update";
            this.Updatebutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Updatebutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Updatebutton.UseVisualStyleBackColor = true;
            this.Updatebutton.Click += new System.EventHandler(this.Updatebutton_Click);
            // 
            // GetRecordbutton
            // 
            this.GetRecordbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GetRecordbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GetRecordbutton.Image = global::SMSS.Properties.Resources.Search_icon_24;
            this.GetRecordbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.GetRecordbutton.Location = new System.Drawing.Point(325, 3);
            this.GetRecordbutton.Name = "GetRecordbutton";
            this.GetRecordbutton.Size = new System.Drawing.Size(155, 47);
            this.GetRecordbutton.TabIndex = 2;
            this.GetRecordbutton.Text = "&View";
            this.GetRecordbutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.GetRecordbutton.UseVisualStyleBackColor = true;
            this.GetRecordbutton.Click += new System.EventHandler(this.GetRecordbutton_Click);
            // 
            // Savebutton
            // 
            this.Savebutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Savebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Savebutton.Image = global::SMSS.Properties.Resources.Save_icon_24;
            this.Savebutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Savebutton.Location = new System.Drawing.Point(164, 3);
            this.Savebutton.Name = "Savebutton";
            this.Savebutton.Size = new System.Drawing.Size(155, 47);
            this.Savebutton.TabIndex = 1;
            this.Savebutton.Text = "&Save";
            this.Savebutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Savebutton.UseVisualStyleBackColor = true;
            this.Savebutton.Click += new System.EventHandler(this.Savebutton_Click);
            // 
            // Addbutton
            // 
            this.Addbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Addbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Addbutton.Image = global::SMSS.Properties.Resources.Add_user_icon_24;
            this.Addbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Addbutton.Location = new System.Drawing.Point(3, 3);
            this.Addbutton.Name = "Addbutton";
            this.Addbutton.Size = new System.Drawing.Size(155, 47);
            this.Addbutton.TabIndex = 0;
            this.Addbutton.Text = "&Add New";
            this.Addbutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Addbutton.UseVisualStyleBackColor = true;
            this.Addbutton.Click += new System.EventHandler(this.Addbutton_Click);
            // 
            // Clearbutton
            // 
            this.Clearbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Clearbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Clearbutton.Image = global::SMSS.Properties.Resources.Knob_Cancel_icon_32;
            this.Clearbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Clearbutton.Location = new System.Drawing.Point(808, 3);
            this.Clearbutton.Name = "Clearbutton";
            this.Clearbutton.Size = new System.Drawing.Size(156, 47);
            this.Clearbutton.TabIndex = 5;
            this.Clearbutton.Text = "&Clear";
            this.Clearbutton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Clearbutton.UseVisualStyleBackColor = true;
            this.Clearbutton.Click += new System.EventHandler(this.Clearbutton_Click);
            // 
            // Centerpanel
            // 
            this.Centerpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Centerpanel.Location = new System.Drawing.Point(0, 129);
            this.Centerpanel.Name = "Centerpanel";
            this.Centerpanel.Size = new System.Drawing.Size(967, 344);
            this.Centerpanel.TabIndex = 2;
            // 
            // Layout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(967, 473);
            this.Controls.Add(this.Centerpanel);
            this.Controls.Add(this.ButtonControlpanel);
            this.Controls.Add(this.Toppanel);
            this.Name = "Layout";
            this.Text = "Layout";
            this.TransparencyKey = System.Drawing.Color.White;
            this.Load += new System.EventHandler(this.Layout_Load);
            this.ButtonControlpanel.ResumeLayout(false);
            this.ButtonControltableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel Toppanel;
        public System.Windows.Forms.Panel ButtonControlpanel;
        public System.Windows.Forms.TableLayoutPanel ButtonControltableLayoutPanel;
        public System.Windows.Forms.Button Delbutton;
        public System.Windows.Forms.Button Updatebutton;
        public System.Windows.Forms.Button GetRecordbutton;
        public System.Windows.Forms.Button Savebutton;
        public System.Windows.Forms.Button Addbutton;
        public System.Windows.Forms.Button Clearbutton;
        public System.Windows.Forms.Panel Centerpanel;
    }
}