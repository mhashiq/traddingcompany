﻿
using SMSS.Screens;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSS
{
    public partial class MainLoginScreen : Form
    {
        public MainLoginScreen()
        {
            Thread t = new Thread(new ThreadStart(SplashScreen));
            t.Start();
            Thread.Sleep(5000);
            InitializeComponent();
            t.Abort();
        }
        public void SplashScreen()
        {
            Application.Run(new SplashScreenForm());
        }
        private void Loginbutton_Click(object sender, EventArgs e)
        {
            MDIMain mdi = new MDIMain();
            this.Hide();
            mdi.Show();
            //this.Close();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ClosepictureBox_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MinimizepictureBox_Click(object sender, EventArgs e)
        {
            if(this.WindowState!=FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void MainLoginScreen_Load(object sender, EventArgs e)
        {
            if(!File.Exists(UtitlityClass.path+"\\connect"))
            {
                Settings s = new Settings();
                this.Hide();
                s.Show();
            }
        }
    }
}
